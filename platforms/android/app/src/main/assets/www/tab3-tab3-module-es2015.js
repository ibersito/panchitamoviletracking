(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["tab3-tab3-module"],{

/***/ "IqiF":
/*!***********************************!*\
  !*** ./src/app/tab3/tab3.page.ts ***!
  \***********************************/
/*! exports provided: Tab3Page */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Tab3Page", function() { return Tab3Page; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _raw_loader_tab3_page_html__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! raw-loader!./tab3.page.html */ "h1hx");
/* harmony import */ var _tab3_page_scss__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./tab3.page.scss */ "nRCe");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "TEn/");
/* harmony import */ var _service_api_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../service/api.service */ "PLN7");
/* harmony import */ var _ionic_native_camera_ngx__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ionic-native/camera/ngx */ "a/9d");
/* harmony import */ var _select_sucursal_select_sucursal_page__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../select-sucursal/select-sucursal.page */ "erPI");








let Tab3Page = class Tab3Page {
    constructor(alertController, nav, loadingController, service, camera, modalController) {
        this.alertController = alertController;
        this.nav = nav;
        this.loadingController = loadingController;
        this.service = service;
        this.camera = camera;
        this.modalController = modalController;
        this.img = "assets/icon.png";
        this.select = "f";
        this.data = { ciudad: "",
            email: "",
            estado: "",
            foto: "",
            id: "",
            moto_id: "",
            nombre: "",
            player_id: "",
            rol: "",
            sucursal: { id: "", nombre: "", ciudad: "", detalle: "", estado: "" },
            sucursal_id: "",
            telefono: "" };
        this.img2 = "assets/camara.png";
        this.select;
    }
    ionViewWillEnter() {
        this.perfil();
    }
    perfil() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const loading = yield this.loadingController.create({
                message: 'Cargando....',
            });
            yield loading.present().then(() => {
                this.service.perfilMotociclista().subscribe((data) => Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
                    this.data = data.data.user;
                    console.log(data);
                    yield loading.dismiss();
                }), err => {
                    console.log(err);
                    loading.dismiss();
                });
            });
        });
    }
    logout() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const alert = yield this.alertController.create({
                cssClass: 'my-custom-class',
                header: 'Confirmar!',
                message: 'Realmente desea salir de la App de Delivery!!',
                buttons: [
                    {
                        text: 'No',
                        role: 'cancel',
                        cssClass: 'secondary',
                        handler: (blah) => {
                            console.log('Confirm Cancel: blah');
                        }
                    }, {
                        text: 'Si',
                        handler: () => {
                            console.log('Confirm Okay');
                            this.nav.navigateRoot('/login');
                            localStorage.clear();
                        }
                    }
                ]
            });
            yield alert.present();
        });
    }
    fotografia() {
        const options = {
            quality: 70,
            destinationType: this.camera.DestinationType.DATA_URL,
            encodingType: this.camera.EncodingType.JPEG,
            mediaType: this.camera.MediaType.PICTURE,
            correctOrientation: true,
            allowEdit: false,
            targetHeight: 800,
            targetWidth: 1020
        };
        this.camera.getPicture(options).then((imageData) => {
            this.imgOficial = "data:image/jpeg;base64," + imageData;
            this.editarFotografia();
        }, (err) => {
            console.log(err);
        });
    }
    editarFotografia() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const loading = yield this.loadingController.create({
                message: 'Cargando....',
            });
            yield loading.present().then(() => {
                this.service.cambiarFotografia(this.imgOficial).subscribe((data) => Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
                    console.log(data);
                    this.success = data;
                    yield loading.dismiss();
                    this.ionViewWillEnter();
                }), err => {
                    console.log(err);
                    // this.errorRequest(err.error.message)
                    loading.dismiss();
                });
            });
        });
    }
    editar() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const modal = yield this.modalController.create({
                component: _select_sucursal_select_sucursal_page__WEBPACK_IMPORTED_MODULE_7__["SelectSucursalPage"]
            });
            modal.onDidDismiss().then(() => {
                this.perfil(); //Update data when closing modal
            });
            return yield modal.present();
        });
    }
};
Tab3Page.ctorParameters = () => [
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["AlertController"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["NavController"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["LoadingController"] },
    { type: _service_api_service__WEBPACK_IMPORTED_MODULE_5__["ApiService"] },
    { type: _ionic_native_camera_ngx__WEBPACK_IMPORTED_MODULE_6__["Camera"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["ModalController"] }
];
Tab3Page = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["Component"])({
        selector: 'app-tab3',
        template: _raw_loader_tab3_page_html__WEBPACK_IMPORTED_MODULE_1__["default"],
        styles: [_tab3_page_scss__WEBPACK_IMPORTED_MODULE_2__["default"]]
    })
], Tab3Page);



/***/ }),

/***/ "OcaV":
/*!*********************************************!*\
  !*** ./src/app/tab3/tab3-routing.module.ts ***!
  \*********************************************/
/*! exports provided: Tab3PageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Tab3PageRoutingModule", function() { return Tab3PageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var _tab3_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./tab3.page */ "IqiF");




const routes = [
    {
        path: '',
        component: _tab3_page__WEBPACK_IMPORTED_MODULE_3__["Tab3Page"],
    }
];
let Tab3PageRoutingModule = class Tab3PageRoutingModule {
};
Tab3PageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
    })
], Tab3PageRoutingModule);



/***/ }),

/***/ "h1hx":
/*!***************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/tab3/tab3.page.html ***!
  \***************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header class=\"ion-no-border\">\r\n  <ion-toolbar color=\"danger\">\r\n    <ion-title class=\"ion-text-center\" color=\"light\">\r\n      Mi Perfil\r\n    </ion-title>\r\n    <ion-buttons slot=\"end\">\r\n      <ion-button color=\"light\" (click)=\"logout()\">\r\n        <ion-icon slot=\"start\" color=\"light\" name=\"exit\"></ion-icon>\r\n      </ion-button>\r\n    </ion-buttons>\r\n   \r\n  </ion-toolbar>\r\n</ion-header>\r\n\r\n<ion-content >\r\n\r\n  <br>\r\n  <div id=\"avatar\" class=\"ion-text-center\">\r\n    <img class=\"img1\" [src]=\"data.foto ? data.foto : img\" alt=\"\"><span></span><img (click)=\"fotografia()\" class=\"img2\" src=\"{{img2}}\" alt=\"\">\r\n  </div>\r\n  <div class=\"ion-text-center\">\r\n   <h2>{{data.nombre}}\r\n  </h2>\r\n   </div>\r\n  <!-- <ion-item>\r\n    <ion-label>Estado</ion-label>\r\n    <ion-select placeholder=\"Select One\" [(ngModel)]=\"select\">\r\n      <ion-select-option value=\"f\">Activo</ion-select-option>\r\n      <ion-select-option value=\"m\">No Activo</ion-select-option>\r\n    </ion-select>\r\n  </ion-item>\r\n\r\n  <ion-item>\r\n    <ion-label position=\"floating\">Nombre</ion-label>\r\n    <ion-input value=\"Iber Arnol Fernandez Mercado\"></ion-input>\r\n  </ion-item>\r\n  <ion-item>\r\n    <ion-label position=\"floating\">Delivery</ion-label>\r\n    <ion-input value=\"Movil-12\"></ion-input>\r\n  </ion-item>\r\n  <ion-item>\r\n    <ion-label position=\"floating\">Delivery</ion-label>\r\n    <ion-input value=\"Movil-12\"></ion-input>\r\n  </ion-item>\r\n  <ion-item>\r\n    <ion-label position=\"floating\">Delivery</ion-label>\r\n    <ion-input value=\"Movil-12\"></ion-input>\r\n  </ion-item>\r\n  <ion-item>\r\n    <ion-label position=\"floating\">Delivery</ion-label>\r\n    <ion-input value=\"Movil-12\"></ion-input>\r\n  </ion-item>\r\n  <ion-item>\r\n    <ion-label position=\"floating\">Delivery</ion-label>\r\n    <ion-input value=\"Movil-12\"></ion-input>\r\n  </ion-item> -->\r\n  <ion-card>\r\n    <!-- <ion-card-content> -->\r\n     <ion-row class=\"ion-text-center\">\r\n       <ion-col size=\"6\">\r\n         <h5>Ciudad</h5>\r\n        <span>{{data.ciudad}}\r\n        </span>\r\n       </ion-col>\r\n       <ion-col size=\"6\">\r\n        <h5>E-mail</h5>\r\n        <span>{{data.email}}\r\n        </span>\r\n       </ion-col>\r\n     </ion-row>\r\n     <ion-row class=\"ion-text-center\">\r\n      <ion-col size=\"6\">\r\n        <h5>Celular</h5>\r\n        <span>{{data.telefono}}\r\n        </span>\r\n      </ion-col>\r\n      <ion-col size=\"6\">\r\n        <h5>Sucursal</h5>\r\n        <span>{{data.sucursal.nombre}}\r\n        </span>\r\n        <div class=\"ion-text-center\">\r\n          <ion-button color=\"danger\" (click)=\"editar()\">\r\n            <ion-icon slot=\"icon-only\" name=\"pencil\"></ion-icon>\r\n          </ion-button>\r\n        </div>\r\n      </ion-col>\r\n    </ion-row>\r\n    \r\n    <!-- </ion-card-content> -->\r\n  </ion-card>\r\n</ion-content>\r\n");

/***/ }),

/***/ "k+ul":
/*!*************************************!*\
  !*** ./src/app/tab3/tab3.module.ts ***!
  \*************************************/
/*! exports provided: Tab3PageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Tab3PageModule", function() { return Tab3PageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @ionic/angular */ "TEn/");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/common */ "ofXK");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/forms */ "3Pt+");
/* harmony import */ var _tab3_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./tab3.page */ "IqiF");
/* harmony import */ var _explore_container_explore_container_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../explore-container/explore-container.module */ "qtYk");
/* harmony import */ var _tab3_routing_module__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./tab3-routing.module */ "OcaV");









let Tab3PageModule = class Tab3PageModule {
};
Tab3PageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["NgModule"])({
        imports: [
            _ionic_angular__WEBPACK_IMPORTED_MODULE_1__["IonicModule"],
            _angular_common__WEBPACK_IMPORTED_MODULE_4__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_5__["FormsModule"],
            _explore_container_explore_container_module__WEBPACK_IMPORTED_MODULE_7__["ExploreContainerComponentModule"],
            _angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild([{ path: '', component: _tab3_page__WEBPACK_IMPORTED_MODULE_6__["Tab3Page"] }]),
            _tab3_routing_module__WEBPACK_IMPORTED_MODULE_8__["Tab3PageRoutingModule"],
        ],
        declarations: [_tab3_page__WEBPACK_IMPORTED_MODULE_6__["Tab3Page"]]
    })
], Tab3PageModule);



/***/ }),

/***/ "nRCe":
/*!*************************************!*\
  !*** ./src/app/tab3/tab3.page.scss ***!
  \*************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("ion-header {\n  background: #e5212b;\n}\n\nh2 {\n  color: #e5212b;\n}\n\n#avatar {\n  padding-left: 30px;\n}\n\n.img1 {\n  box-shadow: 0 0 20px;\n  width: 150px;\n  height: 150px;\n  border-radius: 50px;\n}\n\n.img2 {\n  width: 30px;\n  right: 40px;\n  position: relative;\n  border-radius: 100px;\n}\n\nion-card {\n  box-shadow: 0 0 20px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uXFwuLlxcLi5cXHRhYjMucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUtBO0VBQ0ksbUJBQUE7QUFKSjs7QUFNQTtFQUNJLGNBQUE7QUFISjs7QUFLQTtFQUNJLGtCQUFBO0FBRko7O0FBSUE7RUFDSSxvQkFBQTtFQUNBLFlBQUE7RUFDQSxhQUFBO0VBQ0EsbUJBQUE7QUFESjs7QUFHQTtFQUNJLFdBQUE7RUFDQSxXQUFBO0VBQ0Esa0JBQUE7RUFDQSxvQkFBQTtBQUFKOztBQUVBO0VBQ0ksb0JBQUE7QUFDSiIsImZpbGUiOiJ0YWIzLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi8vIGltZ3tcclxuLy8gICAgIHdpZHRoOiAyMDBweDtcclxuLy8gICAgIGhlaWdodDogMjAwcHg7XHJcbi8vICAgICBib3JkZXItcmFkaXVzOiAyMDBweDtcclxuLy8gfVxyXG5pb24taGVhZGVye1xyXG4gICAgYmFja2dyb3VuZDogI2U1MjEyYjtcclxufVxyXG5oMntcclxuICAgIGNvbG9yOiAjZTUyMTJiXHJcbn1cclxuI2F2YXRhcntcclxuICAgIHBhZGRpbmctbGVmdDogMzBweDtcclxufVxyXG4uaW1nMXtcclxuICAgIGJveC1zaGFkb3c6IDAgMCAyMHB4O1xyXG4gICAgd2lkdGg6IDE1MHB4O1xyXG4gICAgaGVpZ2h0OiAxNTBweDtcclxuICAgIGJvcmRlci1yYWRpdXM6IDUwcHg7XHJcbn1cclxuLmltZzJ7XHJcbiAgICB3aWR0aDogMzBweDtcclxuICAgIHJpZ2h0OiA0MHB4O1xyXG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gICAgYm9yZGVyLXJhZGl1czogMTAwcHg7XHJcbn1cclxuaW9uLWNhcmR7XHJcbiAgICBib3gtc2hhZG93OiAwIDAgMjBweDtcclxufSJdfQ== */");

/***/ })

}]);
//# sourceMappingURL=tab3-tab3-module-es2015.js.map