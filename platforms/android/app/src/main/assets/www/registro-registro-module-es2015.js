(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["registro-registro-module"],{

/***/ "+WFb":
/*!*********************************************!*\
  !*** ./src/app/registro/registro.module.ts ***!
  \*********************************************/
/*! exports provided: RegistroPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RegistroPageModule", function() { return RegistroPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "ofXK");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "3Pt+");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "TEn/");
/* harmony import */ var _registro_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./registro-routing.module */ "q4Mr");
/* harmony import */ var _registro_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./registro.page */ "5Trh");







let RegistroPageModule = class RegistroPageModule {
};
RegistroPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["ReactiveFormsModule"],
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _registro_routing_module__WEBPACK_IMPORTED_MODULE_5__["RegistroPageRoutingModule"]
        ],
        declarations: [_registro_page__WEBPACK_IMPORTED_MODULE_6__["RegistroPage"]]
    })
], RegistroPageModule);



/***/ }),

/***/ "5Trh":
/*!*******************************************!*\
  !*** ./src/app/registro/registro.page.ts ***!
  \*******************************************/
/*! exports provided: RegistroPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RegistroPage", function() { return RegistroPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _raw_loader_registro_page_html__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! raw-loader!./registro.page.html */ "6WTV");
/* harmony import */ var _registro_page_scss__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./registro.page.scss */ "klLB");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/forms */ "3Pt+");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "TEn/");
/* harmony import */ var _service_api_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../service/api.service */ "PLN7");
/* harmony import */ var _validate_sms_register_validate_sms_register_page__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../validate-sms-register/validate-sms-register.page */ "10Jl");








let RegistroPage = class RegistroPage {
    constructor(nav, formBuilder, toastCtrl, service, loading, modalController) {
        this.nav = nav;
        this.formBuilder = formBuilder;
        this.toastCtrl = toastCtrl;
        this.service = service;
        this.loading = loading;
        this.modalController = modalController;
        this.valid = false;
        this.checkError = (controlName, errorName) => {
            return this.form.controls[controlName].hasError(errorName);
        };
        this.cont = 0;
    }
    ngOnInit() {
        this.form = this.formBuilder.group({
            phone: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].compose([_angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].max(79999999), _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].min(59999999), _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].pattern('[0-9]*')])],
        });
    }
    numberOnlyValidation(event) {
        const pattern = /[0-9]/;
        let inputChar = String.fromCharCode(event.charCode);
        if (!pattern.test(inputChar)) {
            // invalid character, prevent input
            event.preventDefault();
        }
    }
    /*
    Funcion send register system client and send page locate
    */
    registerClient() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            this.busqueda();
            if (this.cont == 1) {
                this.errorRequest('Falta ' + (8 - this.text) + ' dígito(s)');
            }
            else if (this.cont == 2) {
                this.errorRequest('El número ingresado no es válido');
            }
            else if (this.cont == 3) {
                this.errorRequest('Ese número no existe en el país case');
            }
            else {
                const loading = yield this.loading.create({
                    message: 'Verificando....',
                });
                yield loading.present().then(() => {
                    this.ci = this.form.value.ci;
                    this.phone = this.form.value.phone;
                    this.service.getSms(this.phone.toString()).subscribe((data) => Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
                        this.success = data.data;
                        console.log(this.success);
                        this.messageExiste = "El número del celular ya esta registrado";
                        const modal = yield this.modalController.create({
                            component: _validate_sms_register_validate_sms_register_page__WEBPACK_IMPORTED_MODULE_7__["ValidateSmsRegisterPage"],
                            componentProps: {
                                telefono: this.success.telefono,
                                message: this.messageExiste,
                                condicion: this.success.existe
                            },
                        });
                        yield loading.dismiss();
                        return yield modal.present();
                        // this.successRequest();
                        // this.nav.navigateRoot('/tabs/tab1');
                    }), err => {
                        this.errorRequest(err.error.data.message);
                        loading.dismiss();
                    });
                });
            }
        });
    }
    successRequest() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const toast = yield this.toastCtrl.create({
                message: 'Bienvenido a Doña Panchita',
                duration: 3000,
                cssClass: 'my-custom-class-success'
            });
            toast.present();
        });
    }
    errorRequest(sms) {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const toast = yield this.toastCtrl.create({
                message: sms,
                duration: 3000,
                cssClass: 'my-custom-class-alert'
            });
            toast.present();
        });
    }
    /*
    Funcion send login page
    */
    sendlogin() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            this.nav.navigateRoot('login');
        });
    }
    busqueda() {
        this.text = 0;
        this.cont = 0;
        var palabra = (this.form.value.phone).toString();
        var arreglo = palabra.split("");
        console.log(arreglo);
        if (arreglo.length === 8) {
            this.secuencial(".", arreglo);
        }
        else if (arreglo.length > 8) {
            this.cont = 3;
        }
        else if (arreglo.length < 8) {
            this.cont = 1;
            this.text = arreglo.length;
        }
    }
    secuencial(value, list) {
        this.cont = 0;
        let found = false;
        let position = -1;
        let index = 0;
        while (!found && index < list.length) {
            if (list[index] == value) {
                found = true;
                position = index;
                this.cont = 2;
            }
            else {
                index += 1;
            }
        }
        return position;
    }
    onChangeTime(e) {
        this.string = (8 - e.detail.value.length);
    }
};
RegistroPage.ctorParameters = () => [
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["NavController"] },
    { type: _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormBuilder"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["ToastController"] },
    { type: _service_api_service__WEBPACK_IMPORTED_MODULE_6__["ApiService"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["LoadingController"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["ModalController"] }
];
RegistroPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["Component"])({
        selector: 'app-registro',
        template: _raw_loader_registro_page_html__WEBPACK_IMPORTED_MODULE_1__["default"],
        styles: [_registro_page_scss__WEBPACK_IMPORTED_MODULE_2__["default"]]
    })
], RegistroPage);



/***/ }),

/***/ "6WTV":
/*!***********************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/registro/registro.page.html ***!
  \***********************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header>\r\n  <ion-toolbar color=\"danger\">\r\n    <ion-buttons color=\"light\" slot=\"start\">\r\n      <ion-back-button></ion-back-button>\r\n    </ion-buttons>\r\n    <ion-title class=\"ion-text-center\">Nuevo Registro</ion-title>\r\n  </ion-toolbar>\r\n</ion-header>\r\n\r\n<ion-content>\r\n  <form [formGroup]=\"form\" >\r\n    <ion-card>\r\n      <ion-list>\r\n        <!-- <ion-label slot=\"start\" class=\"title-input\">Celular</ion-label> -->\r\n        <ion-item>\r\n          <ion-icon slot=\"start\" name=\"phone-portrait-outline\"></ion-icon> <span class=\"codeCountry\">+591 </span>\r\n          <ion-input type=\"number\" (keypress)=\"numberOnlyValidation($event)\" formControlName=\"phone\" inputmode=\"numeric\"\r\n            placeholder=\"Celular\" (ionChange)='onChangeTime($event)'></ion-input>\r\n          <ion-icon class=\"error\" name=\"close-outline\" *ngIf=\"checkError('phone', 'required')\" slot=\"end\"></ion-icon>\r\n          <ion-icon class=\"error\" name=\"close-outline\" *ngIf=\"checkError('phone', 'min')\" slot=\"end\"></ion-icon>\r\n          <ion-icon class=\"error\" name=\"close-outline\" *ngIf=\"checkError('phone', 'max')\" slot=\"end\"></ion-icon>\r\n          <ion-icon class=\"ok\" name=\"checkmark-outline\"\r\n            *ngIf=\"!checkError('phone', 'required') && !checkError('phone', 'min') && !checkError('phone', 'max') && !checkError('phone', 'pattern')\" slot=\"end\"></ion-icon>\r\n        </ion-item>\r\n        <div class=\"validation-errors\">\r\n          <ng-container>\r\n            <div class=\"error-message\" *ngIf=\"checkError('phone', 'pattern')\">Ese número no existe en el país case</div>\r\n            <div class=\"error-message\" *ngIf=\"checkError('phone', 'required')\">*El campo es obligatorio</div>\r\n            <div class=\"error-message\" *ngIf=\"checkError('phone', 'min')\">Falta {{ string }} dígito(s)</div>\r\n            <div class=\"error-message\" *ngIf=\"checkError('phone', 'max')\">Ese número no existe en el país case</div>\r\n          </ng-container>\r\n        </div>\r\n        <br>\r\n    \r\n        <!-- <ion-label slot=\"start\" class=\"title-input\">CI / NIT</ion-label> -->\r\n</ion-list>\r\n    </ion-card>\r\n    <div class=\"ion-text-center\">\r\n      <ion-button [disabled]=\"!form.valid\" (click)=\"registerClient()\" type=\"submit\" color=\"danger\" shape=\"round\">\r\n        Verificar\r\n      </ion-button>\r\n    </div>\r\n  </form>\r\n</ion-content>\r\n");

/***/ }),

/***/ "klLB":
/*!*********************************************!*\
  !*** ./src/app/registro/registro.page.scss ***!
  \*********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".codeCountry {\n  padding: 0px 10px 0px 0px;\n}\n\n.title-input {\n  padding-left: 20px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uXFwuLlxcLi5cXHJlZ2lzdHJvLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNJLHlCQUFBO0FBQ0o7O0FBQ0E7RUFDSSxrQkFBQTtBQUVKIiwiZmlsZSI6InJlZ2lzdHJvLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi5jb2RlQ291bnRyeXtcclxuICAgIHBhZGRpbmc6IDBweCAxMHB4IDBweCAwcHg7XHJcbn1cclxuLnRpdGxlLWlucHV0e1xyXG4gICAgcGFkZGluZy1sZWZ0OiAyMHB4O1xyXG59Il19 */");

/***/ }),

/***/ "q4Mr":
/*!*****************************************************!*\
  !*** ./src/app/registro/registro-routing.module.ts ***!
  \*****************************************************/
/*! exports provided: RegistroPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RegistroPageRoutingModule", function() { return RegistroPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var _registro_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./registro.page */ "5Trh");




const routes = [
    {
        path: '',
        component: _registro_page__WEBPACK_IMPORTED_MODULE_3__["RegistroPage"]
    }
];
let RegistroPageRoutingModule = class RegistroPageRoutingModule {
};
RegistroPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], RegistroPageRoutingModule);



/***/ })

}]);
//# sourceMappingURL=registro-registro-module-es2015.js.map