(function () {
  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

  function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

  function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

  (window["webpackJsonp"] = window["webpackJsonp"] || []).push([["tab1-tab1-module"], {
    /***/
    "8MT7": function MT7(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "<ion-header > \r\n  <ion-toolbar class=\"class-header\">\r\n    <ion-title class=\"ion-text-center\">\r\n      Asignaciones\r\n    </ion-title>\r\n  </ion-toolbar>\r\n</ion-header>\r\n\r\n<ion-content >\r\n\r\n\r\n  <ion-card *ngFor=\"let item of asignacionesUser\" >\r\n    <ion-card-header class=\"ion-text-center\">\r\n      <span class=\"price-producto\" > Cliente</span>\r\n      <p> {{item.cliente.razon_social}}</p>\r\n      <!-- <ion-card-title></ion-card-title> -->\r\n      <br>\r\n      <div>\r\n        <ion-row class=\"ion-text-center\">\r\n          <ion-col size=\"6\"><span class=\"price-producto\"> Direccion</span>\r\n             <p> {{item.direccion}}</p></ion-col>\r\n          <ion-col size=\"6\"><span class=\"price-producto\">Zona</span>\r\n             <p> {{item.cliente.zona}}</p>\r\n            </ion-col>\r\n        </ion-row>\r\n      </div>\r\n      <ion-row class=\"ion-text-center\">\r\n        <ion-col size=\"6\">\r\n         <span class=\"price-producto\"> Fecha :</span>\r\n        \r\n          <p> {{item.fecha_despacho}} </p>\r\n        </ion-col>\r\n        <ion-col size=\"6\">\r\n          <span class=\"price-producto\">Hora despacho</span>\r\n          <p>{{item.hora_despacho}}</p>\r\n        </ion-col>\r\n      </ion-row>\r\n    </ion-card-header>\r\n  \r\n    <ion-card-content align=\"center\" >\r\n      <br>\r\n     <ion-row class=\"ion-text-center\">\r\n       <ion-col size=\"6\">\r\n        <ion-button (click)=\"sendDetallePedido(item)\">\r\n          <!-- <ion-icon slot=\"icon-only\" name=\"map\"> -->\r\n          <!-- </ion-icon>  -->\r\n          Ver Detalle\r\n        </ion-button> \r\n       </ion-col>\r\n       <ion-col size=\"6\">\r\n        <ion-button (click)=\"showLoacte(item)\">\r\n          <!-- <ion-icon slot=\"icon-only\" name=\"map\"> -->\r\n          <!-- </ion-icon>  -->\r\n          Ver Ubicación\r\n        </ion-button>\r\n      </ion-col>\r\n     </ion-row>\r\n     <ion-row class=\"ion-text-center\">\r\n      <ion-col size=\"12\" >\r\n\r\n        <ion-item>\r\n          <ion-label slot=\"start\">{{item.estado == '1' ? 'No entregado':'Entregado'}}</ion-label>\r\n          <ion-toggle [checked]=\"item.estado == '1' ? false:true\" (ionChange)=\"changeEstado($event, item)\" color=\"danger\"></ion-toggle>\r\n  \r\n        </ion-item>\r\n        \r\n        <!-- <ion-button (click)=\"confirmar()\">\r\n           \r\n        </ion-button> -->\r\n       </ion-col>\r\n     </ion-row>\r\n    </ion-card-content>\r\n  </ion-card>\r\n</ion-content>\r\n";
      /***/
    },

    /***/
    "Mzl2": function Mzl2(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "Tab1Page", function () {
        return Tab1Page;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "mrSG");
      /* harmony import */


      var _raw_loader_tab1_page_html__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! raw-loader!./tab1.page.html */
      "8MT7");
      /* harmony import */


      var _tab1_page_scss__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! ./tab1.page.scss */
      "rWyk");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/core */
      "fXoL");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @ionic/angular */
      "TEn/");
      /* harmony import */


      var _ionic_native_launch_navigator_ngx__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! @ionic-native/launch-navigator/ngx */
      "fGQ8");
      /* harmony import */


      var _service_api_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! ../service/api.service */
      "PLN7");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
      /*! @angular/router */
      "tyNb");
      /* harmony import */


      var _ionic_native_local_notifications_ngx__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(
      /*! @ionic-native/local-notifications/ngx */
      "Bg0J");
      /* harmony import */


      var _ionic_native_background_mode_ngx__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(
      /*! @ionic-native/background-mode/ngx */
      "1xeP");
      /* harmony import */


      var _ionic_native_geolocation_ngx__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(
      /*! @ionic-native/geolocation/ngx */
      "Bfh1");
      /* harmony import */


      var firebase__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(
      /*! firebase */
      "JZFu");

      var Tab1Page = /*#__PURE__*/function () {
        function Tab1Page(alertController, toastController, launchNavigator, loadingController, service, router, backgroundMode, platform, localNotifications, geolocation) {
          var _this = this;

          _classCallCheck(this, Tab1Page);

          this.alertController = alertController;
          this.toastController = toastController;
          this.launchNavigator = launchNavigator;
          this.loadingController = loadingController;
          this.service = service;
          this.router = router;
          this.backgroundMode = backgroundMode;
          this.platform = platform;
          this.localNotifications = localNotifications;
          this.geolocation = geolocation;
          this.asignaciones = [];

          this.trackPosition = function () {
            console.log("entre aqui al intervalo");

            _this.showNotification(); //   this.geolocation
            //     .getCurrentPosition({ maximumAge: 3000, timeout: 5000, enableHighAccuracy: true })
            //     .then(position => {
            //       console.log(position);
            //       this.register(position, localStorage.getItem('idMoto'));
            //     })
            //     .catch(error => {
            //       console.log("error", error);
            //     });


            _this.geolocation.watchPosition().subscribe(function (response) {
              return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(_this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee() {
                return regeneratorRuntime.wrap(function _callee$(_context) {
                  while (1) {
                    switch (_context.prev = _context.next) {
                      case 0:
                        console.log(response);
                        this.coords = [response.coords.latitude, response.coords.longitude];
                        this.register(this.coords[0], this.coords[1], localStorage.getItem('idMoto'));

                      case 3:
                      case "end":
                        return _context.stop();
                    }
                  }
                }, _callee, this);
              }));
            });
          };

          platform.ready().then(function () {
            _this.backgroundMode.on('activate').subscribe(function () {
              _this.backgroundMode.disableWebViewOptimizations(); // this.backgroundMode.disableBatteryOptimizations();


              console.log("entre aqui hay asignaciones"); // this.backgroundMode.isScreenOff( () => {
              //   alert('screen off');
              //  });

              clearInterval(_this.interval);
              console.log("cerre el intervalo");
              _this.interval = setInterval(function () {
                // this.backgroundMode.wakeUp();
                console.log("inicie el intervalo");

                _this.trackPosition();
              }, 8000); // }
            }); //   this.backgroundMode.enable();

          });
        }

        _createClass(Tab1Page, [{
          key: "ionViewWillEnter",
          value: function ionViewWillEnter() {
            this.loadAsignacionesPedidos();
            this.condition();
          }
        }, {
          key: "loadAsignacionesPedidos",
          value: function loadAsignacionesPedidos() {
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee3() {
              var _this2 = this;

              var loading;
              return regeneratorRuntime.wrap(function _callee3$(_context3) {
                while (1) {
                  switch (_context3.prev = _context3.next) {
                    case 0:
                      _context3.next = 2;
                      return this.loadingController.create({
                        message: 'Cargando....'
                      });

                    case 2:
                      loading = _context3.sent;
                      _context3.next = 5;
                      return loading.present().then(function () {
                        _this2.service.misAsignaciones().subscribe(function (data) {
                          return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(_this2, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee2() {
                            return regeneratorRuntime.wrap(function _callee2$(_context2) {
                              while (1) {
                                switch (_context2.prev = _context2.next) {
                                  case 0:
                                    this.asignacionesUser = data.data;
                                    console.log(this.asignacionesUser);
                                    _context2.next = 4;
                                    return loading.dismiss();

                                  case 4:
                                  case "end":
                                    return _context2.stop();
                                }
                              }
                            }, _callee2, this);
                          }));
                        }, function (err) {
                          console.log(err);
                          console.log(err);
                          loading.dismiss();
                        });
                      });

                    case 5:
                    case "end":
                      return _context3.stop();
                  }
                }
              }, _callee3, this);
            }));
          }
        }, {
          key: "register",
          value: function register(lat, lng, userId) {
            console.log(userId);
            console.log(lat, lng); // resp.coords.latitude
            // resp.coords.longitude

            firebase__WEBPACK_IMPORTED_MODULE_11__["default"].database().ref('locations/' + userId).update({
              lat: lat,
              lng: lng
            });
            console.log('Ubicando');
          }
        }, {
          key: "changeEstado",
          value: function changeEstado(state, item) {
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee5() {
              var _this3 = this;

              var alert;
              return regeneratorRuntime.wrap(function _callee5$(_context5) {
                while (1) {
                  switch (_context5.prev = _context5.next) {
                    case 0:
                      console.log(state);
                      console.log(item.id);
                      _context5.next = 4;
                      return this.alertController.create({
                        cssClass: 'my-custom-class',
                        header: 'Confirmar!',
                        message: 'Realmente desea marcar como entregado el pedido!!',
                        buttons: [{
                          text: 'No',
                          role: 'cancel',
                          cssClass: 'secondary',
                          handler: function handler(blah) {
                            console.log('Confirm Cancel: blah'); // this.ionViewWillEnter();
                          }
                        }, {
                          text: 'Si',
                          handler: function handler() {
                            console.log('Confirm Okay');

                            _this3.service.actionEntregarPedido(item.id).subscribe(function (data) {
                              return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(_this3, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee4() {
                                return regeneratorRuntime.wrap(function _callee4$(_context4) {
                                  while (1) {
                                    switch (_context4.prev = _context4.next) {
                                      case 0:
                                        this.successPedido = data;
                                        clearInterval(this.interval);
                                        this.ionViewWillEnter();
                                        this.presentToast();
                                        console.log(this.successPedido);

                                      case 5:
                                      case "end":
                                        return _context4.stop();
                                    }
                                  }
                                }, _callee4, this);
                              }));
                            }, function (err) {
                              console.log(err);
                            });
                          }
                        }]
                      });

                    case 4:
                      alert = _context5.sent;
                      _context5.next = 7;
                      return alert.present();

                    case 7:
                    case "end":
                      return _context5.stop();
                  }
                }
              }, _callee5, this);
            }));
          }
        }, {
          key: "presentToast",
          value: function presentToast() {
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee6() {
              var toast;
              return regeneratorRuntime.wrap(function _callee6$(_context6) {
                while (1) {
                  switch (_context6.prev = _context6.next) {
                    case 0:
                      _context6.next = 2;
                      return this.toastController.create({
                        message: 'Su asignación se cambio a entregado puede verlo en su historial de asignaciones.',
                        duration: 2000
                      });

                    case 2:
                      toast = _context6.sent;
                      toast.present();

                    case 4:
                    case "end":
                      return _context6.stop();
                  }
                }
              }, _callee6, this);
            }));
          }
        }, {
          key: "showLoacte",
          value: function showLoacte(item) {
            this.destination = item.latitude + ',' + item.longitude; // console.log(destination);

            var options = {
              // start: 'London, ON',
              // app: this.launchNavigator.APP,
              startName: "Hola I am Here"
            };
            this.launchNavigator.navigate(this.destination, options).then(function (success) {
              return console.log(success);
            }, function (error) {
              return console.log('Error launching navigator', error);
            });
          }
        }, {
          key: "sendDetallePedido",
          value: function sendDetallePedido(item) {
            console.log(item);
            var idPedido = item.id;
            this.router.navigate(["/detalle-producto", {
              idPedido: idPedido
            }]);
          }
        }, {
          key: "condition",
          value: function condition() {
            var _this4 = this;

            // this.StopBackgroundGeo();
            this.service.misAsignaciones().subscribe(function (data) {
              _this4.asignaciones = data.data;

              if (_this4.asignaciones.length === 0) {
                // this.StopBackgroundGeo();
                _this4.backgroundMode.disable();

                clearInterval(_this4.interval);
                console.log('entro al stop');
              } else {
                _this4.backgroundMode.enable();

                console.log('entro al start y activated');
              }
            });
          }
        }, {
          key: "showNotification",
          value: function showNotification() {
            console.log("entre a notificaciones");
            this.localNotifications.schedule({
              text: 'Realizando tracking tienes pedidos asignados cambia su estado para deshabilitar tus notificaciones '
            }); // this.notificationAlreadyReceived = true;
          }
        }]);

        return Tab1Page;
      }();

      Tab1Page.ctorParameters = function () {
        return [{
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["AlertController"]
        }, {
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["ToastController"]
        }, {
          type: _ionic_native_launch_navigator_ngx__WEBPACK_IMPORTED_MODULE_5__["LaunchNavigator"]
        }, {
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["LoadingController"]
        }, {
          type: _service_api_service__WEBPACK_IMPORTED_MODULE_6__["ApiService"]
        }, {
          type: _angular_router__WEBPACK_IMPORTED_MODULE_7__["Router"]
        }, {
          type: _ionic_native_background_mode_ngx__WEBPACK_IMPORTED_MODULE_9__["BackgroundMode"]
        }, {
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["Platform"]
        }, {
          type: _ionic_native_local_notifications_ngx__WEBPACK_IMPORTED_MODULE_8__["LocalNotifications"]
        }, {
          type: _ionic_native_geolocation_ngx__WEBPACK_IMPORTED_MODULE_10__["Geolocation"]
        }];
      };

      Tab1Page = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["Component"])({
        selector: 'app-tab1',
        template: _raw_loader_tab1_page_html__WEBPACK_IMPORTED_MODULE_1__["default"],
        styles: [_tab1_page_scss__WEBPACK_IMPORTED_MODULE_2__["default"]]
      })], Tab1Page);
      /***/
    },

    /***/
    "XOzS": function XOzS(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "Tab1PageRoutingModule", function () {
        return Tab1PageRoutingModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "mrSG");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "fXoL");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/router */
      "tyNb");
      /* harmony import */


      var _tab1_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! ./tab1.page */
      "Mzl2");

      var routes = [{
        path: '',
        component: _tab1_page__WEBPACK_IMPORTED_MODULE_3__["Tab1Page"]
      }];

      var Tab1PageRoutingModule = function Tab1PageRoutingModule() {
        _classCallCheck(this, Tab1PageRoutingModule);
      };

      Tab1PageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
      })], Tab1PageRoutingModule);
      /***/
    },

    /***/
    "rWyk": function rWyk(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "ion-header {\n  background: #e5212b;\n}\n\nion-title {\n  color: white;\n}\n\nion-button {\n  --background: #e5212b;\n}\n\nion-card {\n  box-shadow: 0 0 20px;\n}\n\n.price-producto {\n  color: #0d2e3f;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uXFwuLlxcLi5cXHRhYjEucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0ksbUJBQUE7QUFDSjs7QUFFQTtFQUNJLFlBQUE7QUFDSjs7QUFDQTtFQUNJLHFCQUFBO0FBRUo7O0FBQUE7RUFDQSxvQkFBQTtBQUdBOztBQURBO0VBQ0ksY0FBQTtBQUlKIiwiZmlsZSI6InRhYjEucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiaW9uLWhlYWRlcntcclxuICAgIGJhY2tncm91bmQ6ICNlNTIxMmI7XHJcbn1cclxuXHJcbmlvbi10aXRsZXtcclxuICAgIGNvbG9yOiB3aGl0ZTtcclxufVxyXG5pb24tYnV0dG9ue1xyXG4gICAgLS1iYWNrZ3JvdW5kOiAjZTUyMTJiOztcclxufVxyXG5pb24tY2FyZHtcclxuYm94LXNoYWRvdzogMCAwIDIwcHg7IFxyXG59XHJcbi5wcmljZS1wcm9kdWN0b3tcclxuICAgIGNvbG9yOiAjMGQyZTNmO1xyXG59Il19 */";
      /***/
    },

    /***/
    "tmrb": function tmrb(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "Tab1PageModule", function () {
        return Tab1PageModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "mrSG");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @ionic/angular */
      "TEn/");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/core */
      "fXoL");
      /* harmony import */


      var _angular_common__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/common */
      "ofXK");
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @angular/forms */
      "3Pt+");
      /* harmony import */


      var _tab1_page__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! ./tab1.page */
      "Mzl2");
      /* harmony import */


      var _explore_container_explore_container_module__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! ../explore-container/explore-container.module */
      "qtYk");
      /* harmony import */


      var _tab1_routing_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
      /*! ./tab1-routing.module */
      "XOzS");

      var Tab1PageModule = function Tab1PageModule() {
        _classCallCheck(this, Tab1PageModule);
      };

      Tab1PageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["NgModule"])({
        imports: [_ionic_angular__WEBPACK_IMPORTED_MODULE_1__["IonicModule"], _angular_common__WEBPACK_IMPORTED_MODULE_3__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormsModule"], _explore_container_explore_container_module__WEBPACK_IMPORTED_MODULE_6__["ExploreContainerComponentModule"], _tab1_routing_module__WEBPACK_IMPORTED_MODULE_7__["Tab1PageRoutingModule"]],
        declarations: [_tab1_page__WEBPACK_IMPORTED_MODULE_5__["Tab1Page"]]
      })], Tab1PageModule);
      /***/
    }
  }]);
})();
//# sourceMappingURL=tab1-tab1-module-es5.js.map