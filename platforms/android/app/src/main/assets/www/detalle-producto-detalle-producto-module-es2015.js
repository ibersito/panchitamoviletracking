(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["detalle-producto-detalle-producto-module"],{

/***/ "2Jyt":
/*!*********************************************************************!*\
  !*** ./src/app/detalle-producto/detalle-producto-routing.module.ts ***!
  \*********************************************************************/
/*! exports provided: DetalleProductoPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DetalleProductoPageRoutingModule", function() { return DetalleProductoPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var _detalle_producto_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./detalle-producto.page */ "tcPw");




const routes = [
    {
        path: '',
        component: _detalle_producto_page__WEBPACK_IMPORTED_MODULE_3__["DetalleProductoPage"]
    }
];
let DetalleProductoPageRoutingModule = class DetalleProductoPageRoutingModule {
};
DetalleProductoPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], DetalleProductoPageRoutingModule);



/***/ }),

/***/ "PLN7":
/*!****************************************!*\
  !*** ./src/app/service/api.service.ts ***!
  \****************************************/
/*! exports provided: ApiService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ApiService", function() { return ApiService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "tk/3");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs */ "qCKp");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! rxjs/operators */ "kU1M");





let ApiService = class ApiService {
    constructor(http) {
        this.http = http;
        // path = "https://callcenter.dronebolivia.com/api/";
        this.path = "https://callcenter.rnova-services.com/api/";
        this.items = [];
    }
    loginUsuario(email, password, player_id) {
        this.httpOptions = {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]({
                'Content-Type': 'application/json',
                "Accept": "application/json",
            }),
        };
        var datoaEnviar = {
            "email": email,
            "password": password,
            "player_id": player_id
        };
        return this.http.post(this.path + "moto/login", datoaEnviar, this.httpOptions)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["tap"])((data) => {
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["of"])(data);
        }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["catchError"])((err) => {
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["throwError"])(err);
        }));
    }
    misAsignaciones() {
        this.accesToken = localStorage.getItem('access_token');
        this.idUsuario = localStorage.getItem('idUser');
        this.httpOptions = {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]({
                'Content-Type': 'application/json',
                "Accept": "application/json",
                'Authorization': 'Bearer ' + this.accesToken
            }),
        };
        return this.http.get(this.path + "moto/pedidos", this.httpOptions)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["tap"])((data) => {
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["of"])(data);
        }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["catchError"])((err) => {
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["throwError"])(err);
        }));
    }
    sucursalesPanchita() {
        this.accesToken = localStorage.getItem('access_token');
        this.idUsuario = localStorage.getItem('idUser');
        this.httpOptions = {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]({
                'Content-Type': 'application/json',
                "Accept": "application/json",
                'Authorization': 'Bearer ' + this.accesToken
            }),
        };
        return this.http.get(this.path + "moto/sucursales", this.httpOptions)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["tap"])((data) => {
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["of"])(data);
        }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["catchError"])((err) => {
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["throwError"])(err);
        }));
    }
    actionEntregarPedido(idPedido) {
        this.accesToken = localStorage.getItem('access_token');
        this.userId = localStorage.getItem('idUser');
        this.httpOptions = {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]({
                'Content-Type': 'application/json',
                "Accept": "application/json",
                'Authorization': 'Bearer ' + this.accesToken
            }),
        };
        return this.http.post(this.path + "/moto/entregar-pedido?id=" + idPedido, "", this.httpOptions)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["tap"])((data) => {
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["of"])(data);
        }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["catchError"])((err) => {
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["throwError"])(err);
        }));
    }
    actionVerPedido(idPedido) {
        this.accesToken = localStorage.getItem('access_token');
        this.idUsuario = localStorage.getItem('idUser');
        this.httpOptions = {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]({
                'Content-Type': 'application/json',
                "Accept": "application/json",
                'Authorization': 'Bearer ' + this.accesToken
            }),
        };
        return this.http.get(this.path + "/moto/ver-pedido?id=" + idPedido, this.httpOptions)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["tap"])((data) => {
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["of"])(data);
        }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["catchError"])((err) => {
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["throwError"])(err);
        }));
    }
    perfilMotociclista() {
        this.accesToken = localStorage.getItem('access_token');
        this.idUsuario = localStorage.getItem('idUser');
        this.httpOptions = {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]({
                'Content-Type': 'application/json',
                "Accept": "application/json",
                'Authorization': 'Bearer ' + this.accesToken
            }),
        };
        return this.http.get(this.path + "moto/perfil", this.httpOptions)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["tap"])((data) => {
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["of"])(data);
        }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["catchError"])((err) => {
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["throwError"])(err);
        }));
    }
    cambiarFotografia(foto) {
        this.accesToken = localStorage.getItem('access_token');
        this.userId = localStorage.getItem('idUser');
        this.httpOptions = {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]({
                'Content-Type': 'application/json',
                "Accept": "application/json",
                'Authorization': 'Bearer ' + this.accesToken
            }),
        };
        var datoaEnviar = {
            "foto": foto,
        };
        return this.http.post(this.path + "moto/modificar-foto", datoaEnviar, this.httpOptions)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["tap"])((data) => {
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["of"])(data);
        }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["catchError"])((err) => {
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["throwError"])(err);
        }));
    }
    cambiarSucursal(sucursalId) {
        this.accesToken = localStorage.getItem('access_token');
        this.userId = localStorage.getItem('idUser');
        this.httpOptions = {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]({
                'Content-Type': 'application/json',
                "Accept": "application/json",
                'Authorization': 'Bearer ' + this.accesToken
            }),
        };
        var datoaEnviar = {
            "sucursal_id": sucursalId,
        };
        return this.http.post(this.path + "moto/update", datoaEnviar, this.httpOptions)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["tap"])((data) => {
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["of"])(data);
        }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["catchError"])((err) => {
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["throwError"])(err);
        }));
    }
    getSms(telefono) {
        var datoaEnviar = {
            "telefono": telefono
        };
        return this.http.post(this.path + "moto/login-phone", datoaEnviar, this.httpOptions2)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["tap"])((data) => {
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["of"])(data);
        }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["catchError"])((err) => {
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["throwError"])(err);
        }));
    }
    getRegisterUserNew(telefono, code, player_id = '', nombre = '', ciudad = '', sucursal_id = "") {
        var datoaEnviar = {
            "telefono": telefono,
            "codigo": code,
            "player_id": player_id,
            "nombre": nombre,
            "ciudad": ciudad,
            "sucursal_id": sucursal_id
        };
        return this.http.post(this.path + "moto/login-phone-confirm", datoaEnviar, this.httpOptions2)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["tap"])((data) => {
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["of"])(data);
        }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["catchError"])((err) => {
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["throwError"])(err);
        }));
    }
};
ApiService.ctorParameters = () => [
    { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"] }
];
ApiService = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root'
    })
], ApiService);



/***/ }),

/***/ "gLO6":
/*!*************************************************************!*\
  !*** ./src/app/detalle-producto/detalle-producto.module.ts ***!
  \*************************************************************/
/*! exports provided: DetalleProductoPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DetalleProductoPageModule", function() { return DetalleProductoPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "ofXK");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "3Pt+");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "TEn/");
/* harmony import */ var _detalle_producto_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./detalle-producto-routing.module */ "2Jyt");
/* harmony import */ var _detalle_producto_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./detalle-producto.page */ "tcPw");







let DetalleProductoPageModule = class DetalleProductoPageModule {
};
DetalleProductoPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _detalle_producto_routing_module__WEBPACK_IMPORTED_MODULE_5__["DetalleProductoPageRoutingModule"]
        ],
        declarations: [_detalle_producto_page__WEBPACK_IMPORTED_MODULE_6__["DetalleProductoPage"]]
    })
], DetalleProductoPageModule);



/***/ }),

/***/ "tcPw":
/*!***********************************************************!*\
  !*** ./src/app/detalle-producto/detalle-producto.page.ts ***!
  \***********************************************************/
/*! exports provided: DetalleProductoPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DetalleProductoPage", function() { return DetalleProductoPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _raw_loader_detalle_producto_page_html__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! raw-loader!./detalle-producto.page.html */ "xJwA");
/* harmony import */ var _detalle_producto_page_scss__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./detalle-producto.page.scss */ "xq1Q");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "TEn/");
/* harmony import */ var _service_api_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../service/api.service */ "PLN7");
/* harmony import */ var _ionic_native_in_app_browser_ngx__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @ionic-native/in-app-browser/ngx */ "m/P+");
/* harmony import */ var _ionic_native_call_number_ngx__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @ionic-native/call-number/ngx */ "Wwn5");









let DetalleProductoPage = class DetalleProductoPage {
    constructor(route, loadingController, service, iab, callNumber) {
        this.route = route;
        this.loadingController = loadingController;
        this.service = service;
        this.iab = iab;
        this.callNumber = callNumber;
        this.data = {
            id: "",
            fecha_entrega: "",
            hora_entrega: "",
            fecha_despacho: "",
            hora_despacho: "",
            fecha_entregado: "",
            hora_entregado: "",
            cliente_id: "",
            cliente: {
                id: "",
                nit: "",
                telefono: "",
                zona: "",
                direccion: "",
                razon_social: "",
                zoom: "",
                foto: null,
                latitude: "",
                longitude: ""
            },
            telefono: "",
            zona: "",
            direccion: "",
            latitude: "",
            longitude: "",
            zoom: "",
            instrucciones: "",
            precio_delivery_id: "",
            precio_delivery: {
                id: "",
                nombre: "",
                descripcion: "",
                precio: "",
                estado: ""
            },
            sucursal_delivery_id: "",
            sucursal_delivery: {
                id: "",
                nombre: "",
                ciudad: "",
                detalle: "",
                estado: ""
            },
            tipo_pedido_id: "",
            tipo_pedido: {
                id: "",
                nombre: "",
                descripcion: "",
                estado: ""
            },
            estado: "",
            strEstado: "",
            url_mapa: "",
            detalle: [
                {
                    id: "",
                    producto_id: "",
                    producto: {
                        id: "",
                        producto: "",
                        foto: "",
                        detalle: "",
                        costo: "",
                        estado: "",
                        categoria_producto_id: "",
                        categoria_producto: {
                            id: "",
                            nombre: "",
                            producto: "",
                            estado: ""
                        }
                    },
                    subtotal: "",
                    cantidad: "",
                    observacion: ""
                }
            ],
            total: ""
        };
        this.options = {
            location: 'yes',
            hidden: 'no',
            clearcache: 'yes',
            clearsessioncache: 'yes',
            zoom: 'yes',
            hardwareback: 'yes',
            mediaPlaybackRequiresUserAction: 'no',
            shouldPauseOnSuspend: 'no',
            closebuttoncaption: 'Close',
            disallowoverscroll: 'no',
            toolbar: 'yes',
            enableViewportScale: 'no',
            allowInlineMediaPlayback: 'no',
            presentationstyle: 'pagesheet',
            fullscreen: 'yes',
        };
        this.idPedido = this.route.snapshot.paramMap.get('idPedido');
    }
    ngOnInit() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const loading = yield this.loadingController.create({
                message: 'Cargando....',
            });
            yield loading.present().then(() => {
                this.service.actionVerPedido(this.idPedido).subscribe((data) => Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
                    this.data = data.data;
                    console.log(this.data);
                    yield loading.dismiss();
                }), err => {
                    console.log(err);
                    loading.dismiss();
                });
            });
        });
    }
    watshapp() {
        this.iab.create('https://api.whatsapp.com/send?phone=591' + this.data.telefono + '&text=Hola tu pedido ya esta aqui', `_system`);
    }
    callnumber() {
        this.callNumber.callNumber(this.data.telefono, true)
            .then(res => console.log('Launched dialer!', res))
            .catch(err => console.log('Error launching dialer', err));
    }
};
DetalleProductoPage.ctorParameters = () => [
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["ActivatedRoute"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["LoadingController"] },
    { type: _service_api_service__WEBPACK_IMPORTED_MODULE_6__["ApiService"] },
    { type: _ionic_native_in_app_browser_ngx__WEBPACK_IMPORTED_MODULE_7__["InAppBrowser"] },
    { type: _ionic_native_call_number_ngx__WEBPACK_IMPORTED_MODULE_8__["CallNumber"] }
];
DetalleProductoPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["Component"])({
        selector: 'app-detalle-producto',
        template: _raw_loader_detalle_producto_page_html__WEBPACK_IMPORTED_MODULE_1__["default"],
        styles: [_detalle_producto_page_scss__WEBPACK_IMPORTED_MODULE_2__["default"]]
    })
], DetalleProductoPage);



/***/ }),

/***/ "xJwA":
/*!***************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/detalle-producto/detalle-producto.page.html ***!
  \***************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header>\r\n  <ion-toolbar color=\"danger\">\r\n    <ion-buttons slot=\"start\">\r\n      <ion-back-button color=\"light\"></ion-back-button>\r\n    </ion-buttons>\r\n    <ion-title class=\"ion-text-center\">Pedido {{data.id}}</ion-title>\r\n  </ion-toolbar>\r\n</ion-header>\r\n\r\n<ion-content>\r\n  <ion-card>\r\n    <ion-card-header>\r\n        <div class=\"ion-text-center\">\r\n         <span class=\"price-producto\" > Cliente: {{data.cliente.razon_social}}</span>\r\n        </div>\r\n        <br>\r\n        <div>\r\n          <ion-row class=\"ion-text-center\">\r\n            <ion-col size=\"6\"><span class=\"price-producto\"> Direccion:</span>\r\n               <p> {{data.direccion}}</p></ion-col>\r\n            <ion-col size=\"6\"><span class=\"price-producto\">Zona</span>\r\n               <p> {{data.zona}}</p>\r\n              </ion-col>\r\n          </ion-row>\r\n        </div>\r\n        <br>\r\n        <ion-row>\r\n          <ion-col size=\"6\" class=\"ion-text-center\">\r\n            <ion-button id=\"watshapp\" (click)=\"watshapp()\" >\r\n              <ion-icon slot=\"end\" name=\"logo-whatsapp\"></ion-icon>SMS\r\n            </ion-button>\r\n          </ion-col>\r\n          <ion-col size=\"6\">\r\n            <ion-button id=\"call\" (click)=\"callnumber()\" >\r\n              <ion-icon slot=\"end\" name=\"call\"></ion-icon>Llamar\r\n            </ion-button>\r\n          </ion-col>          \r\n        </ion-row>        \r\n    </ion-card-header>\r\n    <ion-card-content>\r\n    <ion-list *ngFor=\"let item of data.detalle\" >\r\n      <ion-item lines=\"none\">\r\n        <ion-thumbnail slot=\"start\">\r\n          <img [src]=\"item.producto.foto\" />\r\n        </ion-thumbnail>\r\n        <ion-label>\r\n          <span class=\"title-producto\">{{item.producto.detalle}}</span>\r\n          <p class=\"price-producto\">{{item.producto.costo}}Bs.</p>\r\n          <div class=\"ion-text-right\">\r\n            Cantidad: {{item.cantidad}}\r\n            <p  class=\"price-producto\">Sub total: {{item.subtotal}}Bs.</p>\r\n          </div>\r\n        </ion-label>\r\n      </ion-item>\r\n    </ion-list>\r\n    <br>\r\n    <div class=\"ion-text-center\">\r\n      <ion-row>\r\n        <ion-col size=\"6\">\r\n          <h2>Costo Delivery</h2>\r\n          <span>{{data.precio_delivery.precio}}</span>\r\n          \r\n        </ion-col>\r\n        <ion-col size=\"6\">\r\n          <h2>Total Productos</h2>\r\n          <span> {{data.total}} Bs.</span>\r\n        </ion-col>\r\n      </ion-row>\r\n    </div>\r\n    <br>\r\n  \r\n    </ion-card-content> \r\n  </ion-card>\r\n</ion-content>\r\n");

/***/ }),

/***/ "xq1Q":
/*!*************************************************************!*\
  !*** ./src/app/detalle-producto/detalle-producto.page.scss ***!
  \*************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("#watshapp {\n  --border-radius: 50px;\n  --background: #4AC959;\n}\n\n#call {\n  --border-radius: 50px;\n}\n\nion-thumbnail {\n  width: 70px;\n  height: 70px;\n}\n\nion-card {\n  box-shadow: 0 0 30px;\n}\n\nh2 {\n  color: #0d2e3f;\n}\n\nh4 {\n  color: #0d2e3f;\n}\n\n.title-producto {\n  color: #e5212b;\n}\n\n.price-producto {\n  color: #0d2e3f;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uXFwuLlxcLi5cXGRldGFsbGUtcHJvZHVjdG8ucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0kscUJBQUE7RUFDQSxxQkFBQTtBQUNKOztBQUNBO0VBQ0kscUJBQUE7QUFFSjs7QUFBQTtFQUNJLFdBQUE7RUFDQSxZQUFBO0FBR0o7O0FBREE7RUFDSSxvQkFBQTtBQUlKOztBQURBO0VBQ0ksY0FBQTtBQUlKOztBQUZBO0VBQ0ksY0FBQTtBQUtKOztBQUhBO0VBQ0ksY0FBQTtBQU1KOztBQUpBO0VBQ0ksY0FBQTtBQU9KIiwiZmlsZSI6ImRldGFsbGUtcHJvZHVjdG8ucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiI3dhdHNoYXBwe1xyXG4gICAgLS1ib3JkZXItcmFkaXVzOiA1MHB4O1xyXG4gICAgLS1iYWNrZ3JvdW5kOiAjNEFDOTU5OztcclxufVxyXG4jY2FsbHtcclxuICAgIC0tYm9yZGVyLXJhZGl1czogNTBweDtcclxufVxyXG5pb24tdGh1bWJuYWlse1xyXG4gICAgd2lkdGg6IDcwcHg7XHJcbiAgICBoZWlnaHQ6IDcwcHg7XHJcbn1cclxuaW9uLWNhcmR7XHJcbiAgICBib3gtc2hhZG93OiAwIDAgMzBweDtcclxufVxyXG5cclxuaDJ7XHJcbiAgICBjb2xvcjogIzBkMmUzZjtcclxufVxyXG5oNHtcclxuICAgIGNvbG9yOiAjMGQyZTNmO1xyXG59XHJcbi50aXRsZS1wcm9kdWN0b3tcclxuICAgIGNvbG9yOiAjZTUyMTJiO1xyXG59XHJcbi5wcmljZS1wcm9kdWN0b3tcclxuICAgIGNvbG9yOiAjMGQyZTNmO1xyXG59Il19 */");

/***/ })

}]);
//# sourceMappingURL=detalle-producto-detalle-producto-module-es2015.js.map