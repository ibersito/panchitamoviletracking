(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["tab1-tab1-module"],{

/***/ "8MT7":
/*!***************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/tab1/tab1.page.html ***!
  \***************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header > \r\n  <ion-toolbar class=\"class-header\">\r\n    <ion-title class=\"ion-text-center\">\r\n      Asignaciones\r\n    </ion-title>\r\n  </ion-toolbar>\r\n</ion-header>\r\n\r\n<ion-content >\r\n\r\n\r\n  <ion-card *ngFor=\"let item of asignacionesUser\" >\r\n    <ion-card-header class=\"ion-text-center\">\r\n      <span class=\"price-producto\" > Cliente</span>\r\n      <p> {{item.cliente.razon_social}}</p>\r\n      <!-- <ion-card-title></ion-card-title> -->\r\n      <br>\r\n      <div>\r\n        <ion-row class=\"ion-text-center\">\r\n          <ion-col size=\"6\"><span class=\"price-producto\"> Direccion</span>\r\n             <p> {{item.direccion}}</p></ion-col>\r\n          <ion-col size=\"6\"><span class=\"price-producto\">Zona</span>\r\n             <p> {{item.cliente.zona}}</p>\r\n            </ion-col>\r\n        </ion-row>\r\n      </div>\r\n      <ion-row class=\"ion-text-center\">\r\n        <ion-col size=\"6\">\r\n         <span class=\"price-producto\"> Fecha :</span>\r\n        \r\n          <p> {{item.fecha_despacho}} </p>\r\n        </ion-col>\r\n        <ion-col size=\"6\">\r\n          <span class=\"price-producto\">Hora despacho</span>\r\n          <p>{{item.hora_despacho}}</p>\r\n        </ion-col>\r\n      </ion-row>\r\n    </ion-card-header>\r\n  \r\n    <ion-card-content align=\"center\" >\r\n      <br>\r\n     <ion-row class=\"ion-text-center\">\r\n       <ion-col size=\"6\">\r\n        <ion-button (click)=\"sendDetallePedido(item)\">\r\n          <!-- <ion-icon slot=\"icon-only\" name=\"map\"> -->\r\n          <!-- </ion-icon>  -->\r\n          Ver Detalle\r\n        </ion-button> \r\n       </ion-col>\r\n       <ion-col size=\"6\">\r\n        <ion-button (click)=\"showLoacte(item)\">\r\n          <!-- <ion-icon slot=\"icon-only\" name=\"map\"> -->\r\n          <!-- </ion-icon>  -->\r\n          Ver Ubicación\r\n        </ion-button>\r\n      </ion-col>\r\n     </ion-row>\r\n     <ion-row class=\"ion-text-center\">\r\n      <ion-col size=\"12\" >\r\n\r\n        <ion-item>\r\n          <ion-label slot=\"start\">{{item.estado == '1' ? 'No entregado':'Entregado'}}</ion-label>\r\n          <ion-toggle [checked]=\"item.estado == '1' ? false:true\" (ionChange)=\"changeEstado($event, item)\" color=\"danger\"></ion-toggle>\r\n  \r\n        </ion-item>\r\n        \r\n        <!-- <ion-button (click)=\"confirmar()\">\r\n           \r\n        </ion-button> -->\r\n       </ion-col>\r\n     </ion-row>\r\n    </ion-card-content>\r\n  </ion-card>\r\n</ion-content>\r\n");

/***/ }),

/***/ "Mzl2":
/*!***********************************!*\
  !*** ./src/app/tab1/tab1.page.ts ***!
  \***********************************/
/*! exports provided: Tab1Page */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Tab1Page", function() { return Tab1Page; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _raw_loader_tab1_page_html__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! raw-loader!./tab1.page.html */ "8MT7");
/* harmony import */ var _tab1_page_scss__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./tab1.page.scss */ "rWyk");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "TEn/");
/* harmony import */ var _ionic_native_launch_navigator_ngx__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic-native/launch-navigator/ngx */ "fGQ8");
/* harmony import */ var _service_api_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../service/api.service */ "PLN7");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var _ionic_native_local_notifications_ngx__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @ionic-native/local-notifications/ngx */ "Bg0J");
/* harmony import */ var _ionic_native_background_mode_ngx__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @ionic-native/background-mode/ngx */ "1xeP");
/* harmony import */ var _ionic_native_geolocation_ngx__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @ionic-native/geolocation/ngx */ "Bfh1");
/* harmony import */ var firebase__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! firebase */ "JZFu");












let Tab1Page = class Tab1Page {
    constructor(alertController, toastController, launchNavigator, loadingController, service, router, backgroundMode, platform, localNotifications, geolocation) {
        this.alertController = alertController;
        this.toastController = toastController;
        this.launchNavigator = launchNavigator;
        this.loadingController = loadingController;
        this.service = service;
        this.router = router;
        this.backgroundMode = backgroundMode;
        this.platform = platform;
        this.localNotifications = localNotifications;
        this.geolocation = geolocation;
        this.asignaciones = [];
        this.trackPosition = () => {
            console.log("entre aqui al intervalo");
            this.showNotification();
            //   this.geolocation
            //     .getCurrentPosition({ maximumAge: 3000, timeout: 5000, enableHighAccuracy: true })
            //     .then(position => {
            //       console.log(position);
            //       this.register(position, localStorage.getItem('idMoto'));
            //     })
            //     .catch(error => {
            //       console.log("error", error);
            //     });
            this.geolocation.watchPosition().subscribe((response) => Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
                console.log(response);
                this.coords = [response.coords.latitude, response.coords.longitude];
                this.register(this.coords[0], this.coords[1], localStorage.getItem('idMoto'));
            }));
        };
        platform.ready().then(() => {
            this.backgroundMode.on('activate').subscribe(() => {
                this.backgroundMode.disableWebViewOptimizations();
                // this.backgroundMode.disableBatteryOptimizations();
                console.log("entre aqui hay asignaciones");
                // this.backgroundMode.isScreenOff( () => {
                //   alert('screen off');
                //  });
                clearInterval(this.interval);
                console.log("cerre el intervalo");
                this.interval = setInterval(() => {
                    // this.backgroundMode.wakeUp();
                    console.log("inicie el intervalo");
                    this.trackPosition();
                }, 8000);
                // }
            });
            //   this.backgroundMode.enable();
        });
    }
    ionViewWillEnter() {
        this.loadAsignacionesPedidos();
        this.condition();
    }
    loadAsignacionesPedidos() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const loading = yield this.loadingController.create({
                message: 'Cargando....',
            });
            yield loading.present().then(() => {
                this.service.misAsignaciones().subscribe((data) => Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
                    this.asignacionesUser = data.data;
                    console.log(this.asignacionesUser);
                    yield loading.dismiss();
                }), err => {
                    console.log(err);
                    console.log(err);
                    loading.dismiss();
                });
            });
        });
    }
    register(lat, lng, userId) {
        console.log(userId);
        console.log(lat, lng);
        // resp.coords.latitude
        // resp.coords.longitude
        firebase__WEBPACK_IMPORTED_MODULE_11__["default"].database().ref('locations/' + userId).update({
            lat: lat,
            lng: lng
        });
        console.log('Ubicando');
    }
    changeEstado(state, item) {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            console.log(state);
            console.log(item.id);
            const alert = yield this.alertController.create({
                cssClass: 'my-custom-class',
                header: 'Confirmar!',
                message: 'Realmente desea marcar como entregado el pedido!!',
                buttons: [
                    {
                        text: 'No',
                        role: 'cancel',
                        cssClass: 'secondary',
                        handler: (blah) => {
                            console.log('Confirm Cancel: blah');
                            // this.ionViewWillEnter();
                        }
                    },
                    {
                        text: 'Si',
                        handler: () => {
                            console.log('Confirm Okay');
                            this.service.actionEntregarPedido(item.id).subscribe((data) => Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
                                this.successPedido = data;
                                clearInterval(this.interval);
                                this.ionViewWillEnter();
                                this.presentToast();
                                console.log(this.successPedido);
                            }), err => {
                                console.log(err);
                            });
                        }
                    }
                ]
            });
            yield alert.present();
        });
    }
    presentToast() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const toast = yield this.toastController.create({
                message: 'Su asignación se cambio a entregado puede verlo en su historial de asignaciones.',
                duration: 2000
            });
            toast.present();
        });
    }
    showLoacte(item) {
        this.destination = item.latitude + ',' + item.longitude;
        // console.log(destination);
        let options = {
            // start: 'London, ON',
            // app: this.launchNavigator.APP,
            startName: "Hola I am Here"
        };
        this.launchNavigator.navigate(this.destination, options)
            .then(success => console.log(success), error => console.log('Error launching navigator', error));
    }
    sendDetallePedido(item) {
        console.log(item);
        let idPedido = item.id;
        this.router.navigate(["/detalle-producto", { idPedido: idPedido }]);
    }
    condition() {
        // this.StopBackgroundGeo();
        this.service.misAsignaciones().subscribe(data => {
            this.asignaciones = data.data;
            if (this.asignaciones.length === 0) {
                // this.StopBackgroundGeo();
                this.backgroundMode.disable();
                clearInterval(this.interval);
                console.log('entro al stop');
            }
            else {
                this.backgroundMode.enable();
                console.log('entro al start y activated');
            }
        });
    }
    showNotification() {
        console.log("entre a notificaciones");
        this.localNotifications.schedule({
            text: 'Realizando tracking tienes pedidos asignados cambia su estado para deshabilitar tus notificaciones '
        });
        // this.notificationAlreadyReceived = true;
    }
};
Tab1Page.ctorParameters = () => [
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["AlertController"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["ToastController"] },
    { type: _ionic_native_launch_navigator_ngx__WEBPACK_IMPORTED_MODULE_5__["LaunchNavigator"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["LoadingController"] },
    { type: _service_api_service__WEBPACK_IMPORTED_MODULE_6__["ApiService"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_7__["Router"] },
    { type: _ionic_native_background_mode_ngx__WEBPACK_IMPORTED_MODULE_9__["BackgroundMode"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["Platform"] },
    { type: _ionic_native_local_notifications_ngx__WEBPACK_IMPORTED_MODULE_8__["LocalNotifications"] },
    { type: _ionic_native_geolocation_ngx__WEBPACK_IMPORTED_MODULE_10__["Geolocation"] }
];
Tab1Page = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["Component"])({
        selector: 'app-tab1',
        template: _raw_loader_tab1_page_html__WEBPACK_IMPORTED_MODULE_1__["default"],
        styles: [_tab1_page_scss__WEBPACK_IMPORTED_MODULE_2__["default"]]
    })
], Tab1Page);



/***/ }),

/***/ "XOzS":
/*!*********************************************!*\
  !*** ./src/app/tab1/tab1-routing.module.ts ***!
  \*********************************************/
/*! exports provided: Tab1PageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Tab1PageRoutingModule", function() { return Tab1PageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var _tab1_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./tab1.page */ "Mzl2");




const routes = [
    {
        path: '',
        component: _tab1_page__WEBPACK_IMPORTED_MODULE_3__["Tab1Page"],
    }
];
let Tab1PageRoutingModule = class Tab1PageRoutingModule {
};
Tab1PageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
    })
], Tab1PageRoutingModule);



/***/ }),

/***/ "rWyk":
/*!*************************************!*\
  !*** ./src/app/tab1/tab1.page.scss ***!
  \*************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("ion-header {\n  background: #e5212b;\n}\n\nion-title {\n  color: white;\n}\n\nion-button {\n  --background: #e5212b;\n}\n\nion-card {\n  box-shadow: 0 0 20px;\n}\n\n.price-producto {\n  color: #0d2e3f;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uXFwuLlxcLi5cXHRhYjEucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0ksbUJBQUE7QUFDSjs7QUFFQTtFQUNJLFlBQUE7QUFDSjs7QUFDQTtFQUNJLHFCQUFBO0FBRUo7O0FBQUE7RUFDQSxvQkFBQTtBQUdBOztBQURBO0VBQ0ksY0FBQTtBQUlKIiwiZmlsZSI6InRhYjEucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiaW9uLWhlYWRlcntcclxuICAgIGJhY2tncm91bmQ6ICNlNTIxMmI7XHJcbn1cclxuXHJcbmlvbi10aXRsZXtcclxuICAgIGNvbG9yOiB3aGl0ZTtcclxufVxyXG5pb24tYnV0dG9ue1xyXG4gICAgLS1iYWNrZ3JvdW5kOiAjZTUyMTJiOztcclxufVxyXG5pb24tY2FyZHtcclxuYm94LXNoYWRvdzogMCAwIDIwcHg7IFxyXG59XHJcbi5wcmljZS1wcm9kdWN0b3tcclxuICAgIGNvbG9yOiAjMGQyZTNmO1xyXG59Il19 */");

/***/ }),

/***/ "tmrb":
/*!*************************************!*\
  !*** ./src/app/tab1/tab1.module.ts ***!
  \*************************************/
/*! exports provided: Tab1PageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Tab1PageModule", function() { return Tab1PageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @ionic/angular */ "TEn/");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common */ "ofXK");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/forms */ "3Pt+");
/* harmony import */ var _tab1_page__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./tab1.page */ "Mzl2");
/* harmony import */ var _explore_container_explore_container_module__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../explore-container/explore-container.module */ "qtYk");
/* harmony import */ var _tab1_routing_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./tab1-routing.module */ "XOzS");








let Tab1PageModule = class Tab1PageModule {
};
Tab1PageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["NgModule"])({
        imports: [
            _ionic_angular__WEBPACK_IMPORTED_MODULE_1__["IonicModule"],
            _angular_common__WEBPACK_IMPORTED_MODULE_3__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormsModule"],
            _explore_container_explore_container_module__WEBPACK_IMPORTED_MODULE_6__["ExploreContainerComponentModule"],
            _tab1_routing_module__WEBPACK_IMPORTED_MODULE_7__["Tab1PageRoutingModule"]
        ],
        declarations: [_tab1_page__WEBPACK_IMPORTED_MODULE_5__["Tab1Page"]]
    })
], Tab1PageModule);



/***/ })

}]);
//# sourceMappingURL=tab1-tab1-module-es2015.js.map