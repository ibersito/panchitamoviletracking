(function () {
  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

  function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

  function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

  (window["webpackJsonp"] = window["webpackJsonp"] || []).push([["default~registro-registro-module~validate-sms-register-validate-sms-register-module"], {
    /***/
    "10Jl": function Jl(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "ValidateSmsRegisterPage", function () {
        return ValidateSmsRegisterPage;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "mrSG");
      /* harmony import */


      var _raw_loader_validate_sms_register_page_html__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! raw-loader!./validate-sms-register.page.html */
      "Fu2H");
      /* harmony import */


      var _validate_sms_register_page_scss__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! ./validate-sms-register.page.scss */
      "sQoR");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/core */
      "fXoL");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @ionic/angular */
      "TEn/");
      /* harmony import */


      var src_app_service_api_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! src/app/service/api.service */
      "PLN7");
      /* harmony import */


      var _ionic_native_onesignal_ngx__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! @ionic-native/onesignal/ngx */
      "wljF");

      var ValidateSmsRegisterPage = /*#__PURE__*/function () {
        function ValidateSmsRegisterPage(modalCtrl, service2, loading, toastCtrl, nav, loadingController, oneSignal) {
          _classCallCheck(this, ValidateSmsRegisterPage);

          this.modalCtrl = modalCtrl;
          this.service2 = service2;
          this.loading = loading;
          this.toastCtrl = toastCtrl;
          this.nav = nav;
          this.loadingController = loadingController;
          this.oneSignal = oneSignal;
          this.telefono = '';
          this.message = '';
          this.condicion = '';
        }

        _createClass(ValidateSmsRegisterPage, [{
          key: "ngOnInit",
          value: function ngOnInit() {
            var _this = this;

            this.oneSignal.startInit('05406fd8-6a53-43ae-ac38-d416ef79aac9', '785164966576');
            this.oneSignal.getIds().then(function (id) {
              _this.player_id = id.userId;
              console.log(_this.player_id);
            });
            this.oneSignal.inFocusDisplaying(this.oneSignal.OSInFocusDisplayOption.InAppAlert);
            this.oneSignal.handleNotificationReceived().subscribe(function () {});
            this.oneSignal.handleNotificationOpened().subscribe(function () {});
            this.oneSignal.endInit();
            console.log(this.telefono, this.message, this.condicion);
          }
        }, {
          key: "close",
          value: function close() {
            this.modalCtrl.dismiss({
              'dismissed': true
            });
          }
        }, {
          key: "numberOnlyValidation",
          value: function numberOnlyValidation(event) {
            var pattern = /[0-9]/;
            var inputChar = String.fromCharCode(event.charCode);

            if (!pattern.test(inputChar)) {
              // invalid character, prevent input
              event.preventDefault();
            }
          }
        }, {
          key: "validateCode",
          value: function validateCode() {
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee2() {
              var _this2 = this;

              var loading;
              return regeneratorRuntime.wrap(function _callee2$(_context2) {
                while (1) {
                  switch (_context2.prev = _context2.next) {
                    case 0:
                      this.player_id;
                      console.log(this.player_id);
                      this.telOficial = this.telefono.toString();
                      _context2.next = 5;
                      return this.loading.create({
                        message: 'Validando....'
                      });

                    case 5:
                      loading = _context2.sent;
                      _context2.next = 8;
                      return loading.present().then(function () {
                        _this2.service2.getRegisterUserNew(_this2.telOficial, _this2.code, _this2.player_id, _this2.nombre, _this2.ciudad, "5").subscribe(function (data) {
                          return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(_this2, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee() {
                            return regeneratorRuntime.wrap(function _callee$(_context) {
                              while (1) {
                                switch (_context.prev = _context.next) {
                                  case 0:
                                    console.log(data);
                                    this.success = data.data;
                                    console.log(this.success);
                                    localStorage.setItem('access_token', this.success.access_token);
                                    localStorage.setItem('idUser', this.success.user.id);
                                    localStorage.setItem('idMoto', this.success.user.moto_id);
                                    _context.next = 8;
                                    return loading.dismiss();

                                  case 8:
                                    this.modalCtrl.dismiss();
                                    this.successRequest();
                                    this.nav.navigateRoot('/tabs/tab1');

                                  case 11:
                                  case "end":
                                    return _context.stop();
                                }
                              }
                            }, _callee, this);
                          }));
                        }, function (err) {
                          console.log(err);

                          _this2.errorRequest(err.error.data.message);

                          loading.dismiss();
                        });
                      });

                    case 8:
                    case "end":
                      return _context2.stop();
                  }
                }
              }, _callee2, this);
            }));
          }
        }, {
          key: "validateCode2",
          value: function validateCode2() {
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee4() {
              var _this3 = this;

              var loading;
              return regeneratorRuntime.wrap(function _callee4$(_context4) {
                while (1) {
                  switch (_context4.prev = _context4.next) {
                    case 0:
                      this.telOficial = this.telefono.toString();
                      _context4.next = 3;
                      return this.loading.create({
                        message: 'Validando....'
                      });

                    case 3:
                      loading = _context4.sent;
                      _context4.next = 6;
                      return loading.present().then(function () {
                        _this3.service2.getRegisterUserNew(_this3.telOficial, _this3.code, _this3.player_id, _this3.nombre, _this3.ciudad).subscribe(function (data) {
                          return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(_this3, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee3() {
                            return regeneratorRuntime.wrap(function _callee3$(_context3) {
                              while (1) {
                                switch (_context3.prev = _context3.next) {
                                  case 0:
                                    console.log(data);
                                    this.success = data.data;
                                    localStorage.setItem('access_token', this.success.access_token);
                                    localStorage.setItem('idUser', this.success.user.id);
                                    localStorage.setItem('idMoto', this.success.user.moto_id);
                                    _context3.next = 7;
                                    return loading.dismiss();

                                  case 7:
                                    this.modalCtrl.dismiss();
                                    this.successRequest();
                                    this.nav.navigateRoot('/tabs/tab1');

                                  case 10:
                                  case "end":
                                    return _context3.stop();
                                }
                              }
                            }, _callee3, this);
                          }));
                        }, function (err) {
                          console.log(err);

                          _this3.errorRequest(err.error.data.message);

                          loading.dismiss();
                        });
                      });

                    case 6:
                    case "end":
                      return _context4.stop();
                  }
                }
              }, _callee4, this);
            }));
          }
        }, {
          key: "successRequest",
          value: function successRequest() {
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee5() {
              var toast;
              return regeneratorRuntime.wrap(function _callee5$(_context5) {
                while (1) {
                  switch (_context5.prev = _context5.next) {
                    case 0:
                      _context5.next = 2;
                      return this.toastCtrl.create({
                        message: 'Bienvenido a Drone Delivery',
                        duration: 3000,
                        cssClass: 'my-custom-class-success'
                      });

                    case 2:
                      toast = _context5.sent;
                      toast.present();

                    case 4:
                    case "end":
                      return _context5.stop();
                  }
                }
              }, _callee5, this);
            }));
          }
        }, {
          key: "errorRequest",
          value: function errorRequest(sms) {
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee6() {
              var toast;
              return regeneratorRuntime.wrap(function _callee6$(_context6) {
                while (1) {
                  switch (_context6.prev = _context6.next) {
                    case 0:
                      _context6.next = 2;
                      return this.toastCtrl.create({
                        message: sms,
                        duration: 3000,
                        cssClass: 'my-custom-class-alert'
                      });

                    case 2:
                      toast = _context6.sent;
                      toast.present();

                    case 4:
                    case "end":
                      return _context6.stop();
                  }
                }
              }, _callee6, this);
            }));
          }
        }]);

        return ValidateSmsRegisterPage;
      }();

      ValidateSmsRegisterPage.ctorParameters = function () {
        return [{
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["ModalController"]
        }, {
          type: src_app_service_api_service__WEBPACK_IMPORTED_MODULE_5__["ApiService"]
        }, {
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["LoadingController"]
        }, {
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["ToastController"]
        }, {
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["NavController"]
        }, {
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["LoadingController"]
        }, {
          type: _ionic_native_onesignal_ngx__WEBPACK_IMPORTED_MODULE_6__["OneSignal"]
        }];
      };

      ValidateSmsRegisterPage.propDecorators = {
        telefono: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_3__["Input"]
        }],
        message: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_3__["Input"]
        }],
        condicion: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_3__["Input"]
        }]
      };
      ValidateSmsRegisterPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["Component"])({
        selector: 'app-validate-sms-register',
        template: _raw_loader_validate_sms_register_page_html__WEBPACK_IMPORTED_MODULE_1__["default"],
        styles: [_validate_sms_register_page_scss__WEBPACK_IMPORTED_MODULE_2__["default"]]
      })], ValidateSmsRegisterPage);
      /***/
    },

    /***/
    "Fu2H": function Fu2H(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "<ion-header>\r\n  <ion-toolbar color=\"danger\">\r\n      <!-- <ion-buttons color=\"light\" slot=\"start\">\r\n          <ion-back-button ></ion-back-button>\r\n        </ion-buttons> -->\r\n        <ion-buttons color=\"light\" slot=\"start\" (click)=\"close()\">\r\n          <ion-icon  name=\"close\"></ion-icon>\r\n        </ion-buttons>\r\n    <ion-title id=\"font\" class=\"ion-text-center\">Validar</ion-title>\r\n  </ion-toolbar>\r\n</ion-header>\r\n\r\n\r\n<ion-content>\r\n<ion-card>\r\n  <div class=\"ion-text-center\" *ngIf=\"condicion\">\r\n    <h4>{{message}}</h4>\r\n  </div>\r\n  <br>\r\n    <ion-item>\r\n        <ion-icon slot=\"start\" name=\"keypad\"></ion-icon>\r\n        <ion-input type=\"tel\" [(ngModel)]=\"code\" placeholder=\"Código de validación\"></ion-input>\r\n      </ion-item>\r\n      <p class=\"ion-text-center\" style=\"color:red\" *ngIf=\"!code\" >\r\n        Introduzca el codigo enviado al número de celular.\r\n      </p>\r\n      <br>\r\n      <div *ngIf=\"!condicion\">\r\n        <ion-item>\r\n          <ion-icon slot=\"start\" name=\"person-outline\"></ion-icon>\r\n          <ion-input type=\"text\" [(ngModel)]=\"nombre\" placeholder=\"Nombre Completo\"></ion-input>\r\n        </ion-item>\r\n        <p class=\"ion-text-center\" style=\"color:red\" *ngIf=\"!nombre\" >\r\n          Campo nombre es requerido.\r\n        </p>\r\n        <br>\r\n        <ion-item>\r\n          <ion-icon slot=\"start\" name=\"navigate-outline\"></ion-icon>\r\n          <ion-input type=\"text\" [(ngModel)]=\"ciudad\" placeholder=\"Ciudad\"></ion-input>\r\n        </ion-item>\r\n        <p class=\"ion-text-center\" style=\"color:red\" *ngIf=\"!ciudad\" >\r\n          Campo ciudad es requerido.\r\n        </p>\r\n        <br>\r\n    \r\n    \r\n      </div>\r\n</ion-card>\r\n<div class=\"ion-text-center\" *ngIf=\"condicion\" >\r\n  <ion-button [disabled]=\"!code\" (click)=\"validateCode2()\" color=\"danger\" class=\"send\" shape=\"round\">\r\n      <div class=\"ion-text-capitalize font2\">\r\n        Iniciar sesión\r\n      </div>\r\n    </ion-button>\r\n</div>\r\n<div class=\"ion-text-center\" *ngIf=\"!condicion\" >\r\n  <ion-button [disabled]=\"!code || !nombre  || !ciudad \" (click)=\"validateCode()\" color=\"danger\" class=\"send\" shape=\"round\">\r\n      <div class=\"ion-text-capitalize font2\">\r\n        Registrar\r\n      </div>\r\n    </ion-button>\r\n</div>\r\n</ion-content>\r\n\r\n";
      /***/
    },

    /***/
    "PLN7": function PLN7(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "ApiService", function () {
        return ApiService;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "mrSG");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "fXoL");
      /* harmony import */


      var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/common/http */
      "tk/3");
      /* harmony import */


      var rxjs__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! rxjs */
      "qCKp");
      /* harmony import */


      var rxjs_operators__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! rxjs/operators */
      "kU1M");

      var ApiService = /*#__PURE__*/function () {
        function ApiService(http) {
          _classCallCheck(this, ApiService);

          this.http = http; // path = "https://callcenter.dronebolivia.com/api/";

          this.path = "https://callcenter.rnova-services.com/api/";
          this.items = [];
        }

        _createClass(ApiService, [{
          key: "loginUsuario",
          value: function loginUsuario(email, password, player_id) {
            this.httpOptions = {
              headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]({
                'Content-Type': 'application/json',
                "Accept": "application/json"
              })
            };
            var datoaEnviar = {
              "email": email,
              "password": password,
              "player_id": player_id
            };
            return this.http.post(this.path + "moto/login", datoaEnviar, this.httpOptions).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["tap"])(function (data) {
              return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["of"])(data);
            }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["catchError"])(function (err) {
              return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["throwError"])(err);
            }));
          }
        }, {
          key: "misAsignaciones",
          value: function misAsignaciones() {
            this.accesToken = localStorage.getItem('access_token');
            this.idUsuario = localStorage.getItem('idUser');
            this.httpOptions = {
              headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]({
                'Content-Type': 'application/json',
                "Accept": "application/json",
                'Authorization': 'Bearer ' + this.accesToken
              })
            };
            return this.http.get(this.path + "moto/pedidos", this.httpOptions).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["tap"])(function (data) {
              return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["of"])(data);
            }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["catchError"])(function (err) {
              return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["throwError"])(err);
            }));
          }
        }, {
          key: "sucursalesPanchita",
          value: function sucursalesPanchita() {
            this.accesToken = localStorage.getItem('access_token');
            this.idUsuario = localStorage.getItem('idUser');
            this.httpOptions = {
              headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]({
                'Content-Type': 'application/json',
                "Accept": "application/json",
                'Authorization': 'Bearer ' + this.accesToken
              })
            };
            return this.http.get(this.path + "moto/sucursales", this.httpOptions).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["tap"])(function (data) {
              return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["of"])(data);
            }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["catchError"])(function (err) {
              return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["throwError"])(err);
            }));
          }
        }, {
          key: "actionEntregarPedido",
          value: function actionEntregarPedido(idPedido) {
            this.accesToken = localStorage.getItem('access_token');
            this.userId = localStorage.getItem('idUser');
            this.httpOptions = {
              headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]({
                'Content-Type': 'application/json',
                "Accept": "application/json",
                'Authorization': 'Bearer ' + this.accesToken
              })
            };
            return this.http.post(this.path + "/moto/entregar-pedido?id=" + idPedido, "", this.httpOptions).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["tap"])(function (data) {
              return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["of"])(data);
            }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["catchError"])(function (err) {
              return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["throwError"])(err);
            }));
          }
        }, {
          key: "actionVerPedido",
          value: function actionVerPedido(idPedido) {
            this.accesToken = localStorage.getItem('access_token');
            this.idUsuario = localStorage.getItem('idUser');
            this.httpOptions = {
              headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]({
                'Content-Type': 'application/json',
                "Accept": "application/json",
                'Authorization': 'Bearer ' + this.accesToken
              })
            };
            return this.http.get(this.path + "/moto/ver-pedido?id=" + idPedido, this.httpOptions).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["tap"])(function (data) {
              return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["of"])(data);
            }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["catchError"])(function (err) {
              return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["throwError"])(err);
            }));
          }
        }, {
          key: "perfilMotociclista",
          value: function perfilMotociclista() {
            this.accesToken = localStorage.getItem('access_token');
            this.idUsuario = localStorage.getItem('idUser');
            this.httpOptions = {
              headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]({
                'Content-Type': 'application/json',
                "Accept": "application/json",
                'Authorization': 'Bearer ' + this.accesToken
              })
            };
            return this.http.get(this.path + "moto/perfil", this.httpOptions).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["tap"])(function (data) {
              return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["of"])(data);
            }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["catchError"])(function (err) {
              return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["throwError"])(err);
            }));
          }
        }, {
          key: "cambiarFotografia",
          value: function cambiarFotografia(foto) {
            this.accesToken = localStorage.getItem('access_token');
            this.userId = localStorage.getItem('idUser');
            this.httpOptions = {
              headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]({
                'Content-Type': 'application/json',
                "Accept": "application/json",
                'Authorization': 'Bearer ' + this.accesToken
              })
            };
            var datoaEnviar = {
              "foto": foto
            };
            return this.http.post(this.path + "moto/modificar-foto", datoaEnviar, this.httpOptions).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["tap"])(function (data) {
              return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["of"])(data);
            }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["catchError"])(function (err) {
              return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["throwError"])(err);
            }));
          }
        }, {
          key: "cambiarSucursal",
          value: function cambiarSucursal(sucursalId) {
            this.accesToken = localStorage.getItem('access_token');
            this.userId = localStorage.getItem('idUser');
            this.httpOptions = {
              headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]({
                'Content-Type': 'application/json',
                "Accept": "application/json",
                'Authorization': 'Bearer ' + this.accesToken
              })
            };
            var datoaEnviar = {
              "sucursal_id": sucursalId
            };
            return this.http.post(this.path + "moto/update", datoaEnviar, this.httpOptions).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["tap"])(function (data) {
              return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["of"])(data);
            }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["catchError"])(function (err) {
              return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["throwError"])(err);
            }));
          }
        }, {
          key: "getSms",
          value: function getSms(telefono) {
            var datoaEnviar = {
              "telefono": telefono
            };
            return this.http.post(this.path + "moto/login-phone", datoaEnviar, this.httpOptions2).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["tap"])(function (data) {
              return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["of"])(data);
            }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["catchError"])(function (err) {
              return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["throwError"])(err);
            }));
          }
        }, {
          key: "getRegisterUserNew",
          value: function getRegisterUserNew(telefono, code) {
            var player_id = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : '';
            var nombre = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : '';
            var ciudad = arguments.length > 4 && arguments[4] !== undefined ? arguments[4] : '';
            var sucursal_id = arguments.length > 5 && arguments[5] !== undefined ? arguments[5] : "";
            var datoaEnviar = {
              "telefono": telefono,
              "codigo": code,
              "player_id": player_id,
              "nombre": nombre,
              "ciudad": ciudad,
              "sucursal_id": sucursal_id
            };
            return this.http.post(this.path + "moto/login-phone-confirm", datoaEnviar, this.httpOptions2).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["tap"])(function (data) {
              return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["of"])(data);
            }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["catchError"])(function (err) {
              return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["throwError"])(err);
            }));
          }
        }]);

        return ApiService;
      }();

      ApiService.ctorParameters = function () {
        return [{
          type: _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"]
        }];
      };

      ApiService = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root'
      })], ApiService);
      /***/
    },

    /***/
    "sQoR": function sQoR(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJ2YWxpZGF0ZS1zbXMtcmVnaXN0ZXIucGFnZS5zY3NzIn0= */";
      /***/
    }
  }]);
})();
//# sourceMappingURL=default~registro-registro-module~validate-sms-register-validate-sms-register-module-es5.js.map