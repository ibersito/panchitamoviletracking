(function () {
  function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

  function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

  (window["webpackJsonp"] = window["webpackJsonp"] || []).push([["registro-registro-module"], {
    /***/
    "+WFb": function WFb(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "RegistroPageModule", function () {
        return RegistroPageModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "mrSG");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "fXoL");
      /* harmony import */


      var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/common */
      "ofXK");
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/forms */
      "3Pt+");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @ionic/angular */
      "TEn/");
      /* harmony import */


      var _registro_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! ./registro-routing.module */
      "q4Mr");
      /* harmony import */


      var _registro_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! ./registro.page */
      "5Trh");

      var RegistroPageModule = function RegistroPageModule() {
        _classCallCheck(this, RegistroPageModule);
      };

      RegistroPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_forms__WEBPACK_IMPORTED_MODULE_3__["ReactiveFormsModule"], _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"], _registro_routing_module__WEBPACK_IMPORTED_MODULE_5__["RegistroPageRoutingModule"]],
        declarations: [_registro_page__WEBPACK_IMPORTED_MODULE_6__["RegistroPage"]]
      })], RegistroPageModule);
      /***/
    },

    /***/
    "5Trh": function Trh(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "RegistroPage", function () {
        return RegistroPage;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "mrSG");
      /* harmony import */


      var _raw_loader_registro_page_html__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! raw-loader!./registro.page.html */
      "6WTV");
      /* harmony import */


      var _registro_page_scss__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! ./registro.page.scss */
      "klLB");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/core */
      "fXoL");
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @angular/forms */
      "3Pt+");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! @ionic/angular */
      "TEn/");
      /* harmony import */


      var _service_api_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! ../service/api.service */
      "PLN7");
      /* harmony import */


      var _validate_sms_register_validate_sms_register_page__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
      /*! ../validate-sms-register/validate-sms-register.page */
      "10Jl");

      var RegistroPage = /*#__PURE__*/function () {
        function RegistroPage(nav, formBuilder, toastCtrl, service, loading, modalController) {
          var _this = this;

          _classCallCheck(this, RegistroPage);

          this.nav = nav;
          this.formBuilder = formBuilder;
          this.toastCtrl = toastCtrl;
          this.service = service;
          this.loading = loading;
          this.modalController = modalController;
          this.valid = false;

          this.checkError = function (controlName, errorName) {
            return _this.form.controls[controlName].hasError(errorName);
          };

          this.cont = 0;
        }

        _createClass(RegistroPage, [{
          key: "ngOnInit",
          value: function ngOnInit() {
            this.form = this.formBuilder.group({
              phone: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].compose([_angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].max(79999999), _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].min(59999999), _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].pattern('[0-9]*')])]
            });
          }
        }, {
          key: "numberOnlyValidation",
          value: function numberOnlyValidation(event) {
            var pattern = /[0-9]/;
            var inputChar = String.fromCharCode(event.charCode);

            if (!pattern.test(inputChar)) {
              // invalid character, prevent input
              event.preventDefault();
            }
          }
          /*
          Funcion send register system client and send page locate
          */

        }, {
          key: "registerClient",
          value: function registerClient() {
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee2() {
              var _this2 = this;

              var loading;
              return regeneratorRuntime.wrap(function _callee2$(_context2) {
                while (1) {
                  switch (_context2.prev = _context2.next) {
                    case 0:
                      this.busqueda();

                      if (!(this.cont == 1)) {
                        _context2.next = 5;
                        break;
                      }

                      this.errorRequest('Falta ' + (8 - this.text) + ' dígito(s)');
                      _context2.next = 18;
                      break;

                    case 5:
                      if (!(this.cont == 2)) {
                        _context2.next = 9;
                        break;
                      }

                      this.errorRequest('El número ingresado no es válido');
                      _context2.next = 18;
                      break;

                    case 9:
                      if (!(this.cont == 3)) {
                        _context2.next = 13;
                        break;
                      }

                      this.errorRequest('Ese número no existe en el país case');
                      _context2.next = 18;
                      break;

                    case 13:
                      _context2.next = 15;
                      return this.loading.create({
                        message: 'Verificando....'
                      });

                    case 15:
                      loading = _context2.sent;
                      _context2.next = 18;
                      return loading.present().then(function () {
                        _this2.ci = _this2.form.value.ci;
                        _this2.phone = _this2.form.value.phone;

                        _this2.service.getSms(_this2.phone.toString()).subscribe(function (data) {
                          return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(_this2, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee() {
                            var modal;
                            return regeneratorRuntime.wrap(function _callee$(_context) {
                              while (1) {
                                switch (_context.prev = _context.next) {
                                  case 0:
                                    this.success = data.data;
                                    console.log(this.success);
                                    this.messageExiste = "El número del celular ya esta registrado";
                                    _context.next = 5;
                                    return this.modalController.create({
                                      component: _validate_sms_register_validate_sms_register_page__WEBPACK_IMPORTED_MODULE_7__["ValidateSmsRegisterPage"],
                                      componentProps: {
                                        telefono: this.success.telefono,
                                        message: this.messageExiste,
                                        condicion: this.success.existe
                                      }
                                    });

                                  case 5:
                                    modal = _context.sent;
                                    _context.next = 8;
                                    return loading.dismiss();

                                  case 8:
                                    _context.next = 10;
                                    return modal.present();

                                  case 10:
                                    return _context.abrupt("return", _context.sent);

                                  case 11:
                                  case "end":
                                    return _context.stop();
                                }
                              }
                            }, _callee, this);
                          }));
                        }, function (err) {
                          _this2.errorRequest(err.error.data.message);

                          loading.dismiss();
                        });
                      });

                    case 18:
                    case "end":
                      return _context2.stop();
                  }
                }
              }, _callee2, this);
            }));
          }
        }, {
          key: "successRequest",
          value: function successRequest() {
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee3() {
              var toast;
              return regeneratorRuntime.wrap(function _callee3$(_context3) {
                while (1) {
                  switch (_context3.prev = _context3.next) {
                    case 0:
                      _context3.next = 2;
                      return this.toastCtrl.create({
                        message: 'Bienvenido a Doña Panchita',
                        duration: 3000,
                        cssClass: 'my-custom-class-success'
                      });

                    case 2:
                      toast = _context3.sent;
                      toast.present();

                    case 4:
                    case "end":
                      return _context3.stop();
                  }
                }
              }, _callee3, this);
            }));
          }
        }, {
          key: "errorRequest",
          value: function errorRequest(sms) {
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee4() {
              var toast;
              return regeneratorRuntime.wrap(function _callee4$(_context4) {
                while (1) {
                  switch (_context4.prev = _context4.next) {
                    case 0:
                      _context4.next = 2;
                      return this.toastCtrl.create({
                        message: sms,
                        duration: 3000,
                        cssClass: 'my-custom-class-alert'
                      });

                    case 2:
                      toast = _context4.sent;
                      toast.present();

                    case 4:
                    case "end":
                      return _context4.stop();
                  }
                }
              }, _callee4, this);
            }));
          }
          /*
          Funcion send login page
          */

        }, {
          key: "sendlogin",
          value: function sendlogin() {
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee5() {
              return regeneratorRuntime.wrap(function _callee5$(_context5) {
                while (1) {
                  switch (_context5.prev = _context5.next) {
                    case 0:
                      this.nav.navigateRoot('login');

                    case 1:
                    case "end":
                      return _context5.stop();
                  }
                }
              }, _callee5, this);
            }));
          }
        }, {
          key: "busqueda",
          value: function busqueda() {
            this.text = 0;
            this.cont = 0;
            var palabra = this.form.value.phone.toString();
            var arreglo = palabra.split("");
            console.log(arreglo);

            if (arreglo.length === 8) {
              this.secuencial(".", arreglo);
            } else if (arreglo.length > 8) {
              this.cont = 3;
            } else if (arreglo.length < 8) {
              this.cont = 1;
              this.text = arreglo.length;
            }
          }
        }, {
          key: "secuencial",
          value: function secuencial(value, list) {
            this.cont = 0;
            var found = false;
            var position = -1;
            var index = 0;

            while (!found && index < list.length) {
              if (list[index] == value) {
                found = true;
                position = index;
                this.cont = 2;
              } else {
                index += 1;
              }
            }

            return position;
          }
        }, {
          key: "onChangeTime",
          value: function onChangeTime(e) {
            this.string = 8 - e.detail.value.length;
          }
        }]);

        return RegistroPage;
      }();

      RegistroPage.ctorParameters = function () {
        return [{
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["NavController"]
        }, {
          type: _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormBuilder"]
        }, {
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["ToastController"]
        }, {
          type: _service_api_service__WEBPACK_IMPORTED_MODULE_6__["ApiService"]
        }, {
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["LoadingController"]
        }, {
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["ModalController"]
        }];
      };

      RegistroPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["Component"])({
        selector: 'app-registro',
        template: _raw_loader_registro_page_html__WEBPACK_IMPORTED_MODULE_1__["default"],
        styles: [_registro_page_scss__WEBPACK_IMPORTED_MODULE_2__["default"]]
      })], RegistroPage);
      /***/
    },

    /***/
    "6WTV": function WTV(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "<ion-header>\r\n  <ion-toolbar color=\"danger\">\r\n    <ion-buttons color=\"light\" slot=\"start\">\r\n      <ion-back-button></ion-back-button>\r\n    </ion-buttons>\r\n    <ion-title class=\"ion-text-center\">Nuevo Registro</ion-title>\r\n  </ion-toolbar>\r\n</ion-header>\r\n\r\n<ion-content>\r\n  <form [formGroup]=\"form\" >\r\n    <ion-card>\r\n      <ion-list>\r\n        <!-- <ion-label slot=\"start\" class=\"title-input\">Celular</ion-label> -->\r\n        <ion-item>\r\n          <ion-icon slot=\"start\" name=\"phone-portrait-outline\"></ion-icon> <span class=\"codeCountry\">+591 </span>\r\n          <ion-input type=\"number\" (keypress)=\"numberOnlyValidation($event)\" formControlName=\"phone\" inputmode=\"numeric\"\r\n            placeholder=\"Celular\" (ionChange)='onChangeTime($event)'></ion-input>\r\n          <ion-icon class=\"error\" name=\"close-outline\" *ngIf=\"checkError('phone', 'required')\" slot=\"end\"></ion-icon>\r\n          <ion-icon class=\"error\" name=\"close-outline\" *ngIf=\"checkError('phone', 'min')\" slot=\"end\"></ion-icon>\r\n          <ion-icon class=\"error\" name=\"close-outline\" *ngIf=\"checkError('phone', 'max')\" slot=\"end\"></ion-icon>\r\n          <ion-icon class=\"ok\" name=\"checkmark-outline\"\r\n            *ngIf=\"!checkError('phone', 'required') && !checkError('phone', 'min') && !checkError('phone', 'max') && !checkError('phone', 'pattern')\" slot=\"end\"></ion-icon>\r\n        </ion-item>\r\n        <div class=\"validation-errors\">\r\n          <ng-container>\r\n            <div class=\"error-message\" *ngIf=\"checkError('phone', 'pattern')\">Ese número no existe en el país case</div>\r\n            <div class=\"error-message\" *ngIf=\"checkError('phone', 'required')\">*El campo es obligatorio</div>\r\n            <div class=\"error-message\" *ngIf=\"checkError('phone', 'min')\">Falta {{ string }} dígito(s)</div>\r\n            <div class=\"error-message\" *ngIf=\"checkError('phone', 'max')\">Ese número no existe en el país case</div>\r\n          </ng-container>\r\n        </div>\r\n        <br>\r\n    \r\n        <!-- <ion-label slot=\"start\" class=\"title-input\">CI / NIT</ion-label> -->\r\n</ion-list>\r\n    </ion-card>\r\n    <div class=\"ion-text-center\">\r\n      <ion-button [disabled]=\"!form.valid\" (click)=\"registerClient()\" type=\"submit\" color=\"danger\" shape=\"round\">\r\n        Verificar\r\n      </ion-button>\r\n    </div>\r\n  </form>\r\n</ion-content>\r\n";
      /***/
    },

    /***/
    "klLB": function klLB(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = ".codeCountry {\n  padding: 0px 10px 0px 0px;\n}\n\n.title-input {\n  padding-left: 20px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uXFwuLlxcLi5cXHJlZ2lzdHJvLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNJLHlCQUFBO0FBQ0o7O0FBQ0E7RUFDSSxrQkFBQTtBQUVKIiwiZmlsZSI6InJlZ2lzdHJvLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi5jb2RlQ291bnRyeXtcclxuICAgIHBhZGRpbmc6IDBweCAxMHB4IDBweCAwcHg7XHJcbn1cclxuLnRpdGxlLWlucHV0e1xyXG4gICAgcGFkZGluZy1sZWZ0OiAyMHB4O1xyXG59Il19 */";
      /***/
    },

    /***/
    "q4Mr": function q4Mr(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "RegistroPageRoutingModule", function () {
        return RegistroPageRoutingModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "mrSG");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "fXoL");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/router */
      "tyNb");
      /* harmony import */


      var _registro_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! ./registro.page */
      "5Trh");

      var routes = [{
        path: '',
        component: _registro_page__WEBPACK_IMPORTED_MODULE_3__["RegistroPage"]
      }];

      var RegistroPageRoutingModule = function RegistroPageRoutingModule() {
        _classCallCheck(this, RegistroPageRoutingModule);
      };

      RegistroPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
      })], RegistroPageRoutingModule);
      /***/
    }
  }]);
})();
//# sourceMappingURL=registro-registro-module-es5.js.map