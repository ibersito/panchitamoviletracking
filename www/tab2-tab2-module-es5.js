(function () {
  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

  function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

  function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

  (window["webpackJsonp"] = window["webpackJsonp"] || []).push([["tab2-tab2-module"], {
    /***/
    "EGAO": function EGAO(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "ion-header {\n  background: #e5212b;\n}\n\nion-title {\n  color: white;\n}\n\nion-button {\n  --background: #e5212b;\n}\n\nion-card {\n  box-shadow: 0 0 20px;\n}\n\n.price-producto {\n  color: #0d2e3f;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uXFwuLlxcLi5cXHRhYjIucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0ksbUJBQUE7QUFDSjs7QUFFQTtFQUNJLFlBQUE7QUFDSjs7QUFDQTtFQUNJLHFCQUFBO0FBRUo7O0FBQUE7RUFDSSxvQkFBQTtBQUdKOztBQURJO0VBQ0ksY0FBQTtBQUlSIiwiZmlsZSI6InRhYjIucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiaW9uLWhlYWRlcntcclxuICAgIGJhY2tncm91bmQ6ICNlNTIxMmI7XHJcbn1cclxuXHJcbmlvbi10aXRsZXtcclxuICAgIGNvbG9yOiB3aGl0ZTtcclxufVxyXG5pb24tYnV0dG9ue1xyXG4gICAgLS1iYWNrZ3JvdW5kOiAjZTUyMTJiOztcclxufVxyXG5pb24tY2FyZHtcclxuICAgIGJveC1zaGFkb3c6IDAgMCAyMHB4OyBcclxuICAgIH1cclxuICAgIC5wcmljZS1wcm9kdWN0b3tcclxuICAgICAgICBjb2xvcjogIzBkMmUzZjtcclxuICAgIH1cclxuIl19 */";
      /***/
    },

    /***/
    "JZ9U": function JZ9U(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "Tab2Page", function () {
        return Tab2Page;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "mrSG");
      /* harmony import */


      var _raw_loader_tab2_page_html__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! raw-loader!./tab2.page.html */
      "e9nj");
      /* harmony import */


      var _tab2_page_scss__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! ./tab2.page.scss */
      "EGAO");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/core */
      "fXoL");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @ionic/angular */
      "TEn/");
      /* harmony import */


      var _service_api_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! ../service/api.service */
      "PLN7");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! @angular/router */
      "tyNb");
      /* harmony import */


      var _ionic_native_geolocation_ngx__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
      /*! @ionic-native/geolocation/ngx */
      "Bfh1");

      var Tab2Page = /*#__PURE__*/function () {
        function Tab2Page(alertController, nav, loadingController, service, router, geolocation) {
          _classCallCheck(this, Tab2Page);

          this.alertController = alertController;
          this.nav = nav;
          this.loadingController = loadingController;
          this.service = service;
          this.router = router;
          this.geolocation = geolocation;
          this.historial = [{
            cliente: "Cliente: Dr- Pablo Aranda",
            direccion: "Avenida Petrolera km 3 1/2",
            pedido: " 1 cuartos Pollos ",
            fecha: "07/6/2020 16:42"
          }, {
            cliente: "Cliente: Dr- Gonzalo Pereda",
            direccion: "Avenida Suecia km 3 1/2",
            pedido: " 4 cuartos Pollos ",
            fecha: "07/6/2020 15:42"
          }, {
            cliente: "Cliente: Ing- Jorge Veizaga",
            direccion: "Avenida Libertador km 3 1/2",
            pedido: " 4 cuartos Pollos ",
            fecha: "07/6/2020 14:42"
          }];
          this.data = {
            ciudad: "",
            email: "",
            estado: "",
            foto: "",
            id: "",
            moto_id: "",
            nombre: "",
            player_id: "",
            rol: "",
            sucursal: {
              id: "",
              nombre: "",
              ciudad: "",
              detalle: "",
              estado: ""
            },
            sucursal_id: "",
            telefono: ""
          };
        }

        _createClass(Tab2Page, [{
          key: "ionViewWillEnter",
          value: function ionViewWillEnter() {
            this.perfil();
          }
        }, {
          key: "perfil",
          value: function perfil() {
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee2() {
              var _this = this;

              var loading;
              return regeneratorRuntime.wrap(function _callee2$(_context2) {
                while (1) {
                  switch (_context2.prev = _context2.next) {
                    case 0:
                      _context2.next = 2;
                      return this.loadingController.create({
                        message: 'Cargando....'
                      });

                    case 2:
                      loading = _context2.sent;
                      _context2.next = 5;
                      return loading.present().then(function () {
                        _this.service.perfilMotociclista().subscribe(function (data) {
                          return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(_this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee() {
                            return regeneratorRuntime.wrap(function _callee$(_context) {
                              while (1) {
                                switch (_context.prev = _context.next) {
                                  case 0:
                                    this.asignacionesUser = data.data.pedidos;
                                    console.log(this.asignacionesUser);
                                    _context.next = 4;
                                    return loading.dismiss();

                                  case 4:
                                  case "end":
                                    return _context.stop();
                                }
                              }
                            }, _callee, this);
                          }));
                        }, function (err) {
                          console.log(err);
                          loading.dismiss();
                        });
                      });

                    case 5:
                    case "end":
                      return _context2.stop();
                  }
                }
              }, _callee2, this);
            }));
          }
        }, {
          key: "sendDetallePedido",
          value: function sendDetallePedido(item) {
            console.log(item);
            var idPedido = item.id;
            this.router.navigate(["/detalle-producto", {
              idPedido: idPedido
            }]);
          }
        }]);

        return Tab2Page;
      }();

      Tab2Page.ctorParameters = function () {
        return [{
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["AlertController"]
        }, {
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["NavController"]
        }, {
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["LoadingController"]
        }, {
          type: _service_api_service__WEBPACK_IMPORTED_MODULE_5__["ApiService"]
        }, {
          type: _angular_router__WEBPACK_IMPORTED_MODULE_6__["Router"]
        }, {
          type: _ionic_native_geolocation_ngx__WEBPACK_IMPORTED_MODULE_7__["Geolocation"]
        }];
      };

      Tab2Page = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["Component"])({
        selector: 'app-tab2',
        template: _raw_loader_tab2_page_html__WEBPACK_IMPORTED_MODULE_1__["default"],
        styles: [_tab2_page_scss__WEBPACK_IMPORTED_MODULE_2__["default"]]
      })], Tab2Page);
      /***/
    },

    /***/
    "TUkU": function TUkU(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "Tab2PageModule", function () {
        return Tab2PageModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "mrSG");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @ionic/angular */
      "TEn/");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/core */
      "fXoL");
      /* harmony import */


      var _angular_common__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/common */
      "ofXK");
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @angular/forms */
      "3Pt+");
      /* harmony import */


      var _tab2_page__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! ./tab2.page */
      "JZ9U");
      /* harmony import */


      var _explore_container_explore_container_module__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! ../explore-container/explore-container.module */
      "qtYk");
      /* harmony import */


      var _tab2_routing_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
      /*! ./tab2-routing.module */
      "UDmF");

      var Tab2PageModule = function Tab2PageModule() {
        _classCallCheck(this, Tab2PageModule);
      };

      Tab2PageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["NgModule"])({
        imports: [_ionic_angular__WEBPACK_IMPORTED_MODULE_1__["IonicModule"], _angular_common__WEBPACK_IMPORTED_MODULE_3__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormsModule"], _explore_container_explore_container_module__WEBPACK_IMPORTED_MODULE_6__["ExploreContainerComponentModule"], _tab2_routing_module__WEBPACK_IMPORTED_MODULE_7__["Tab2PageRoutingModule"]],
        declarations: [_tab2_page__WEBPACK_IMPORTED_MODULE_5__["Tab2Page"]]
      })], Tab2PageModule);
      /***/
    },

    /***/
    "UDmF": function UDmF(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "Tab2PageRoutingModule", function () {
        return Tab2PageRoutingModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "mrSG");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "fXoL");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/router */
      "tyNb");
      /* harmony import */


      var _tab2_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! ./tab2.page */
      "JZ9U");

      var routes = [{
        path: '',
        component: _tab2_page__WEBPACK_IMPORTED_MODULE_3__["Tab2Page"]
      }];

      var Tab2PageRoutingModule = function Tab2PageRoutingModule() {
        _classCallCheck(this, Tab2PageRoutingModule);
      };

      Tab2PageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
      })], Tab2PageRoutingModule);
      /***/
    },

    /***/
    "e9nj": function e9nj(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "<ion-header >\r\n  <ion-toolbar color=\"danger\">\r\n    <ion-title class=\"ion-text-center\" color=\"light\">\r\n      Historial\r\n    </ion-title>\r\n  </ion-toolbar>\r\n</ion-header>\r\n\r\n<ion-content>\r\n  \r\n\r\n  <ion-card *ngFor=\"let item of asignacionesUser\" >\r\n    <!-- <ion-card-header>\r\n      <ion-card-subtitle>Cliente: {{item.cliente.razon_social}}</ion-card-subtitle>\r\n      <br>\r\n      <ion-row>\r\n        <ion-col size=\"6\">\r\n          Fecha : {{item.fecha_despacho}}\r\n          <p>Hora despacho: {{item.hora_despacho}}</p>\r\n        </ion-col>\r\n        <ion-col size=\"6\">\r\n          <p>Dirección de entrega:</p>\r\n          <span>{{item.cliente.direccion}}</span>\r\n        </ion-col>\r\n      </ion-row>\r\n    </ion-card-header> -->\r\n    <ion-card-header class=\"ion-text-center\">\r\n      <span class=\"price-producto\" > Cliente</span>\r\n      <p> {{item.cliente.razon_social}}</p>\r\n      <!-- <ion-card-title></ion-card-title> -->\r\n      <br>\r\n      <div>\r\n        <ion-row class=\"ion-text-center\">\r\n          <ion-col size=\"6\"><span class=\"price-producto\"> Direccion</span>\r\n             <p> {{item.direccion}}</p></ion-col>\r\n          <ion-col size=\"6\"><span class=\"price-producto\">Zona</span>\r\n             <p> {{item.cliente.zona}}</p>\r\n            </ion-col>\r\n        </ion-row>\r\n      </div>\r\n      <ion-row class=\"ion-text-center\">\r\n        <ion-col size=\"6\">\r\n         <span class=\"price-producto\"> Fecha :</span>\r\n        \r\n          <p > {{item.fecha_despacho}} </p>\r\n        </ion-col>\r\n        <ion-col size=\"6\">\r\n          <span class=\"price-producto\">Hora despacho</span>\r\n          <p>{{item.hora_despacho}}</p>\r\n        </ion-col>\r\n      </ion-row>\r\n    </ion-card-header>\r\n\r\n    \r\n    <ion-card-content>\r\n      <br>\r\n     <ion-row>\r\n       <ion-col size=\"6\">\r\n        <ion-button (click)=\"sendDetallePedido(item)\">\r\n          <!-- <ion-icon slot=\"icon-only\" name=\"map\"> -->\r\n          <!-- </ion-icon>  -->\r\n          Ver Detalle\r\n        </ion-button> \r\n       </ion-col>\r\n       <ion-col size=\"6\">\r\n        <!-- <ion-button (click)=\"showLoacte(item)\">\r\n          strEstado: \"Terminado\"\r\n          \r\n          Ver Ubicación\r\n        </ion-button> -->\r\n        <h3>{{item.strEstado == \"Terminado\" ? 'Entrega completada' : 'null'}}</h3>\r\n      </ion-col>\r\n     </ion-row>\r\n     <ion-row>\r\n      <ion-col size=\"12\" class=\"ion-text-center\">\r\n\r\n        <!-- <ion-item>\r\n          <ion-label slot=\"start\">{{item.estado == '1' ? 'No entregado':'Entregado'}}</ion-label>\r\n          <ion-toggle [checked]=\"item.estado == '1' ? false:true\" (ionChange)=\"changeEstado($event, item)\" color=\"danger\"></ion-toggle>\r\n  \r\n        </ion-item> -->\r\n        \r\n        <!-- <ion-button (click)=\"confirmar()\">\r\n           \r\n        </ion-button> -->\r\n       </ion-col>\r\n     </ion-row>\r\n    </ion-card-content>\r\n  </ion-card>\r\n</ion-content>\r\n";
      /***/
    }
  }]);
})();
//# sourceMappingURL=tab2-tab2-module-es5.js.map