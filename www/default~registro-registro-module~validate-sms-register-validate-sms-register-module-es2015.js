(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["default~registro-registro-module~validate-sms-register-validate-sms-register-module"],{

/***/ "10Jl":
/*!*********************************************************************!*\
  !*** ./src/app/validate-sms-register/validate-sms-register.page.ts ***!
  \*********************************************************************/
/*! exports provided: ValidateSmsRegisterPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ValidateSmsRegisterPage", function() { return ValidateSmsRegisterPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _raw_loader_validate_sms_register_page_html__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! raw-loader!./validate-sms-register.page.html */ "Fu2H");
/* harmony import */ var _validate_sms_register_page_scss__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./validate-sms-register.page.scss */ "sQoR");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "TEn/");
/* harmony import */ var src_app_service_api_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! src/app/service/api.service */ "PLN7");
/* harmony import */ var _ionic_native_onesignal_ngx__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ionic-native/onesignal/ngx */ "wljF");







let ValidateSmsRegisterPage = class ValidateSmsRegisterPage {
    constructor(modalCtrl, service2, loading, toastCtrl, nav, loadingController, oneSignal) {
        this.modalCtrl = modalCtrl;
        this.service2 = service2;
        this.loading = loading;
        this.toastCtrl = toastCtrl;
        this.nav = nav;
        this.loadingController = loadingController;
        this.oneSignal = oneSignal;
        this.telefono = '';
        this.message = '';
        this.condicion = '';
    }
    ngOnInit() {
        this.oneSignal.startInit('05406fd8-6a53-43ae-ac38-d416ef79aac9', '785164966576');
        this.oneSignal.getIds().then((id) => {
            this.player_id = id.userId;
            console.log(this.player_id);
        });
        this.oneSignal.inFocusDisplaying(this.oneSignal.OSInFocusDisplayOption.InAppAlert);
        this.oneSignal.handleNotificationReceived().subscribe(() => {
        });
        this.oneSignal.handleNotificationOpened().subscribe(() => {
        });
        this.oneSignal.endInit();
        console.log(this.telefono, this.message, this.condicion);
    }
    close() {
        this.modalCtrl.dismiss({
            'dismissed': true
        });
    }
    numberOnlyValidation(event) {
        const pattern = /[0-9]/;
        let inputChar = String.fromCharCode(event.charCode);
        if (!pattern.test(inputChar)) {
            // invalid character, prevent input
            event.preventDefault();
        }
    }
    validateCode() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            this.player_id;
            console.log(this.player_id);
            this.telOficial = this.telefono.toString();
            const loading = yield this.loading.create({
                message: 'Validando....',
            });
            yield loading.present().then(() => {
                this.service2.getRegisterUserNew(this.telOficial, this.code, this.player_id, this.nombre, this.ciudad, "5").subscribe((data) => Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
                    console.log(data);
                    this.success = data.data;
                    console.log(this.success);
                    localStorage.setItem('access_token', this.success.access_token);
                    localStorage.setItem('idUser', this.success.user.id);
                    localStorage.setItem('idMoto', this.success.user.moto_id);
                    yield loading.dismiss();
                    this.modalCtrl.dismiss();
                    this.successRequest();
                    this.nav.navigateRoot('/tabs/tab1');
                }), err => {
                    console.log(err);
                    this.errorRequest(err.error.data.message);
                    loading.dismiss();
                });
            });
        });
    }
    validateCode2() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            this.telOficial = this.telefono.toString();
            const loading = yield this.loading.create({
                message: 'Validando....',
            });
            yield loading.present().then(() => {
                this.service2.getRegisterUserNew(this.telOficial, this.code, this.player_id, this.nombre, this.ciudad).subscribe((data) => Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
                    console.log(data);
                    this.success = data.data;
                    localStorage.setItem('access_token', this.success.access_token);
                    localStorage.setItem('idUser', this.success.user.id);
                    localStorage.setItem('idMoto', this.success.user.moto_id);
                    yield loading.dismiss();
                    this.modalCtrl.dismiss();
                    this.successRequest();
                    this.nav.navigateRoot('/tabs/tab1');
                }), err => {
                    console.log(err);
                    this.errorRequest(err.error.data.message);
                    loading.dismiss();
                });
            });
        });
    }
    successRequest() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const toast = yield this.toastCtrl.create({
                message: 'Bienvenido a Drone Delivery',
                duration: 3000,
                cssClass: 'my-custom-class-success'
            });
            toast.present();
        });
    }
    errorRequest(sms) {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const toast = yield this.toastCtrl.create({
                message: sms,
                duration: 3000,
                cssClass: 'my-custom-class-alert'
            });
            toast.present();
        });
    }
};
ValidateSmsRegisterPage.ctorParameters = () => [
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["ModalController"] },
    { type: src_app_service_api_service__WEBPACK_IMPORTED_MODULE_5__["ApiService"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["LoadingController"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["ToastController"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["NavController"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["LoadingController"] },
    { type: _ionic_native_onesignal_ngx__WEBPACK_IMPORTED_MODULE_6__["OneSignal"] }
];
ValidateSmsRegisterPage.propDecorators = {
    telefono: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_3__["Input"] }],
    message: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_3__["Input"] }],
    condicion: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_3__["Input"] }]
};
ValidateSmsRegisterPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["Component"])({
        selector: 'app-validate-sms-register',
        template: _raw_loader_validate_sms_register_page_html__WEBPACK_IMPORTED_MODULE_1__["default"],
        styles: [_validate_sms_register_page_scss__WEBPACK_IMPORTED_MODULE_2__["default"]]
    })
], ValidateSmsRegisterPage);



/***/ }),

/***/ "Fu2H":
/*!*************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/validate-sms-register/validate-sms-register.page.html ***!
  \*************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header>\r\n  <ion-toolbar color=\"danger\">\r\n      <!-- <ion-buttons color=\"light\" slot=\"start\">\r\n          <ion-back-button ></ion-back-button>\r\n        </ion-buttons> -->\r\n        <ion-buttons color=\"light\" slot=\"start\" (click)=\"close()\">\r\n          <ion-icon  name=\"close\"></ion-icon>\r\n        </ion-buttons>\r\n    <ion-title id=\"font\" class=\"ion-text-center\">Validar</ion-title>\r\n  </ion-toolbar>\r\n</ion-header>\r\n\r\n\r\n<ion-content>\r\n<ion-card>\r\n  <div class=\"ion-text-center\" *ngIf=\"condicion\">\r\n    <h4>{{message}}</h4>\r\n  </div>\r\n  <br>\r\n    <ion-item>\r\n        <ion-icon slot=\"start\" name=\"keypad\"></ion-icon>\r\n        <ion-input type=\"tel\" [(ngModel)]=\"code\" placeholder=\"Código de validación\"></ion-input>\r\n      </ion-item>\r\n      <p class=\"ion-text-center\" style=\"color:red\" *ngIf=\"!code\" >\r\n        Introduzca el codigo enviado al número de celular.\r\n      </p>\r\n      <br>\r\n      <div *ngIf=\"!condicion\">\r\n        <ion-item>\r\n          <ion-icon slot=\"start\" name=\"person-outline\"></ion-icon>\r\n          <ion-input type=\"text\" [(ngModel)]=\"nombre\" placeholder=\"Nombre Completo\"></ion-input>\r\n        </ion-item>\r\n        <p class=\"ion-text-center\" style=\"color:red\" *ngIf=\"!nombre\" >\r\n          Campo nombre es requerido.\r\n        </p>\r\n        <br>\r\n        <ion-item>\r\n          <ion-icon slot=\"start\" name=\"navigate-outline\"></ion-icon>\r\n          <ion-input type=\"text\" [(ngModel)]=\"ciudad\" placeholder=\"Ciudad\"></ion-input>\r\n        </ion-item>\r\n        <p class=\"ion-text-center\" style=\"color:red\" *ngIf=\"!ciudad\" >\r\n          Campo ciudad es requerido.\r\n        </p>\r\n        <br>\r\n    \r\n    \r\n      </div>\r\n</ion-card>\r\n<div class=\"ion-text-center\" *ngIf=\"condicion\" >\r\n  <ion-button [disabled]=\"!code\" (click)=\"validateCode2()\" color=\"danger\" class=\"send\" shape=\"round\">\r\n      <div class=\"ion-text-capitalize font2\">\r\n        Iniciar sesión\r\n      </div>\r\n    </ion-button>\r\n</div>\r\n<div class=\"ion-text-center\" *ngIf=\"!condicion\" >\r\n  <ion-button [disabled]=\"!code || !nombre  || !ciudad \" (click)=\"validateCode()\" color=\"danger\" class=\"send\" shape=\"round\">\r\n      <div class=\"ion-text-capitalize font2\">\r\n        Registrar\r\n      </div>\r\n    </ion-button>\r\n</div>\r\n</ion-content>\r\n\r\n");

/***/ }),

/***/ "PLN7":
/*!****************************************!*\
  !*** ./src/app/service/api.service.ts ***!
  \****************************************/
/*! exports provided: ApiService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ApiService", function() { return ApiService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "tk/3");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs */ "qCKp");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! rxjs/operators */ "kU1M");





let ApiService = class ApiService {
    constructor(http) {
        this.http = http;
        // path = "https://callcenter.dronebolivia.com/api/";
        this.path = "https://callcenter.rnova-services.com/api/";
        this.items = [];
    }
    loginUsuario(email, password, player_id) {
        this.httpOptions = {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]({
                'Content-Type': 'application/json',
                "Accept": "application/json",
            }),
        };
        var datoaEnviar = {
            "email": email,
            "password": password,
            "player_id": player_id
        };
        return this.http.post(this.path + "moto/login", datoaEnviar, this.httpOptions)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["tap"])((data) => {
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["of"])(data);
        }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["catchError"])((err) => {
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["throwError"])(err);
        }));
    }
    misAsignaciones() {
        this.accesToken = localStorage.getItem('access_token');
        this.idUsuario = localStorage.getItem('idUser');
        this.httpOptions = {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]({
                'Content-Type': 'application/json',
                "Accept": "application/json",
                'Authorization': 'Bearer ' + this.accesToken
            }),
        };
        return this.http.get(this.path + "moto/pedidos", this.httpOptions)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["tap"])((data) => {
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["of"])(data);
        }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["catchError"])((err) => {
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["throwError"])(err);
        }));
    }
    sucursalesPanchita() {
        this.accesToken = localStorage.getItem('access_token');
        this.idUsuario = localStorage.getItem('idUser');
        this.httpOptions = {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]({
                'Content-Type': 'application/json',
                "Accept": "application/json",
                'Authorization': 'Bearer ' + this.accesToken
            }),
        };
        return this.http.get(this.path + "moto/sucursales", this.httpOptions)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["tap"])((data) => {
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["of"])(data);
        }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["catchError"])((err) => {
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["throwError"])(err);
        }));
    }
    actionEntregarPedido(idPedido) {
        this.accesToken = localStorage.getItem('access_token');
        this.userId = localStorage.getItem('idUser');
        this.httpOptions = {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]({
                'Content-Type': 'application/json',
                "Accept": "application/json",
                'Authorization': 'Bearer ' + this.accesToken
            }),
        };
        return this.http.post(this.path + "/moto/entregar-pedido?id=" + idPedido, "", this.httpOptions)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["tap"])((data) => {
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["of"])(data);
        }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["catchError"])((err) => {
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["throwError"])(err);
        }));
    }
    actionVerPedido(idPedido) {
        this.accesToken = localStorage.getItem('access_token');
        this.idUsuario = localStorage.getItem('idUser');
        this.httpOptions = {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]({
                'Content-Type': 'application/json',
                "Accept": "application/json",
                'Authorization': 'Bearer ' + this.accesToken
            }),
        };
        return this.http.get(this.path + "/moto/ver-pedido?id=" + idPedido, this.httpOptions)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["tap"])((data) => {
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["of"])(data);
        }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["catchError"])((err) => {
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["throwError"])(err);
        }));
    }
    perfilMotociclista() {
        this.accesToken = localStorage.getItem('access_token');
        this.idUsuario = localStorage.getItem('idUser');
        this.httpOptions = {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]({
                'Content-Type': 'application/json',
                "Accept": "application/json",
                'Authorization': 'Bearer ' + this.accesToken
            }),
        };
        return this.http.get(this.path + "moto/perfil", this.httpOptions)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["tap"])((data) => {
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["of"])(data);
        }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["catchError"])((err) => {
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["throwError"])(err);
        }));
    }
    cambiarFotografia(foto) {
        this.accesToken = localStorage.getItem('access_token');
        this.userId = localStorage.getItem('idUser');
        this.httpOptions = {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]({
                'Content-Type': 'application/json',
                "Accept": "application/json",
                'Authorization': 'Bearer ' + this.accesToken
            }),
        };
        var datoaEnviar = {
            "foto": foto,
        };
        return this.http.post(this.path + "moto/modificar-foto", datoaEnviar, this.httpOptions)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["tap"])((data) => {
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["of"])(data);
        }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["catchError"])((err) => {
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["throwError"])(err);
        }));
    }
    cambiarSucursal(sucursalId) {
        this.accesToken = localStorage.getItem('access_token');
        this.userId = localStorage.getItem('idUser');
        this.httpOptions = {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]({
                'Content-Type': 'application/json',
                "Accept": "application/json",
                'Authorization': 'Bearer ' + this.accesToken
            }),
        };
        var datoaEnviar = {
            "sucursal_id": sucursalId,
        };
        return this.http.post(this.path + "moto/update", datoaEnviar, this.httpOptions)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["tap"])((data) => {
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["of"])(data);
        }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["catchError"])((err) => {
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["throwError"])(err);
        }));
    }
    getSms(telefono) {
        var datoaEnviar = {
            "telefono": telefono
        };
        return this.http.post(this.path + "moto/login-phone", datoaEnviar, this.httpOptions2)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["tap"])((data) => {
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["of"])(data);
        }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["catchError"])((err) => {
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["throwError"])(err);
        }));
    }
    getRegisterUserNew(telefono, code, player_id = '', nombre = '', ciudad = '', sucursal_id = "") {
        var datoaEnviar = {
            "telefono": telefono,
            "codigo": code,
            "player_id": player_id,
            "nombre": nombre,
            "ciudad": ciudad,
            "sucursal_id": sucursal_id
        };
        return this.http.post(this.path + "moto/login-phone-confirm", datoaEnviar, this.httpOptions2)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["tap"])((data) => {
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["of"])(data);
        }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["catchError"])((err) => {
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["throwError"])(err);
        }));
    }
};
ApiService.ctorParameters = () => [
    { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"] }
];
ApiService = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root'
    })
], ApiService);



/***/ }),

/***/ "sQoR":
/*!***********************************************************************!*\
  !*** ./src/app/validate-sms-register/validate-sms-register.page.scss ***!
  \***********************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJ2YWxpZGF0ZS1zbXMtcmVnaXN0ZXIucGFnZS5zY3NzIn0= */");

/***/ })

}]);
//# sourceMappingURL=default~registro-registro-module~validate-sms-register-validate-sms-register-module-es2015.js.map