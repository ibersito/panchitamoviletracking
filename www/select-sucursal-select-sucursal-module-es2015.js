(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["select-sucursal-select-sucursal-module"],{

/***/ "PLN7":
/*!****************************************!*\
  !*** ./src/app/service/api.service.ts ***!
  \****************************************/
/*! exports provided: ApiService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ApiService", function() { return ApiService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "tk/3");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs */ "qCKp");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! rxjs/operators */ "kU1M");





let ApiService = class ApiService {
    constructor(http) {
        this.http = http;
        // path = "https://callcenter.dronebolivia.com/api/";
        this.path = "https://callcenter.rnova-services.com/api/";
        this.items = [];
    }
    loginUsuario(email, password, player_id) {
        this.httpOptions = {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]({
                'Content-Type': 'application/json',
                "Accept": "application/json",
            }),
        };
        var datoaEnviar = {
            "email": email,
            "password": password,
            "player_id": player_id
        };
        return this.http.post(this.path + "moto/login", datoaEnviar, this.httpOptions)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["tap"])((data) => {
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["of"])(data);
        }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["catchError"])((err) => {
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["throwError"])(err);
        }));
    }
    misAsignaciones() {
        this.accesToken = localStorage.getItem('access_token');
        this.idUsuario = localStorage.getItem('idUser');
        this.httpOptions = {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]({
                'Content-Type': 'application/json',
                "Accept": "application/json",
                'Authorization': 'Bearer ' + this.accesToken
            }),
        };
        return this.http.get(this.path + "moto/pedidos", this.httpOptions)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["tap"])((data) => {
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["of"])(data);
        }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["catchError"])((err) => {
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["throwError"])(err);
        }));
    }
    sucursalesPanchita() {
        this.accesToken = localStorage.getItem('access_token');
        this.idUsuario = localStorage.getItem('idUser');
        this.httpOptions = {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]({
                'Content-Type': 'application/json',
                "Accept": "application/json",
                'Authorization': 'Bearer ' + this.accesToken
            }),
        };
        return this.http.get(this.path + "moto/sucursales", this.httpOptions)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["tap"])((data) => {
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["of"])(data);
        }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["catchError"])((err) => {
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["throwError"])(err);
        }));
    }
    actionEntregarPedido(idPedido) {
        this.accesToken = localStorage.getItem('access_token');
        this.userId = localStorage.getItem('idUser');
        this.httpOptions = {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]({
                'Content-Type': 'application/json',
                "Accept": "application/json",
                'Authorization': 'Bearer ' + this.accesToken
            }),
        };
        return this.http.post(this.path + "/moto/entregar-pedido?id=" + idPedido, "", this.httpOptions)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["tap"])((data) => {
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["of"])(data);
        }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["catchError"])((err) => {
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["throwError"])(err);
        }));
    }
    actionVerPedido(idPedido) {
        this.accesToken = localStorage.getItem('access_token');
        this.idUsuario = localStorage.getItem('idUser');
        this.httpOptions = {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]({
                'Content-Type': 'application/json',
                "Accept": "application/json",
                'Authorization': 'Bearer ' + this.accesToken
            }),
        };
        return this.http.get(this.path + "/moto/ver-pedido?id=" + idPedido, this.httpOptions)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["tap"])((data) => {
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["of"])(data);
        }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["catchError"])((err) => {
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["throwError"])(err);
        }));
    }
    perfilMotociclista() {
        this.accesToken = localStorage.getItem('access_token');
        this.idUsuario = localStorage.getItem('idUser');
        this.httpOptions = {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]({
                'Content-Type': 'application/json',
                "Accept": "application/json",
                'Authorization': 'Bearer ' + this.accesToken
            }),
        };
        return this.http.get(this.path + "moto/perfil", this.httpOptions)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["tap"])((data) => {
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["of"])(data);
        }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["catchError"])((err) => {
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["throwError"])(err);
        }));
    }
    cambiarFotografia(foto) {
        this.accesToken = localStorage.getItem('access_token');
        this.userId = localStorage.getItem('idUser');
        this.httpOptions = {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]({
                'Content-Type': 'application/json',
                "Accept": "application/json",
                'Authorization': 'Bearer ' + this.accesToken
            }),
        };
        var datoaEnviar = {
            "foto": foto,
        };
        return this.http.post(this.path + "moto/modificar-foto", datoaEnviar, this.httpOptions)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["tap"])((data) => {
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["of"])(data);
        }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["catchError"])((err) => {
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["throwError"])(err);
        }));
    }
    cambiarSucursal(sucursalId) {
        this.accesToken = localStorage.getItem('access_token');
        this.userId = localStorage.getItem('idUser');
        this.httpOptions = {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]({
                'Content-Type': 'application/json',
                "Accept": "application/json",
                'Authorization': 'Bearer ' + this.accesToken
            }),
        };
        var datoaEnviar = {
            "sucursal_id": sucursalId,
        };
        return this.http.post(this.path + "moto/update", datoaEnviar, this.httpOptions)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["tap"])((data) => {
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["of"])(data);
        }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["catchError"])((err) => {
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["throwError"])(err);
        }));
    }
    getSms(telefono) {
        var datoaEnviar = {
            "telefono": telefono
        };
        return this.http.post(this.path + "moto/login-phone", datoaEnviar, this.httpOptions2)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["tap"])((data) => {
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["of"])(data);
        }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["catchError"])((err) => {
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["throwError"])(err);
        }));
    }
    getRegisterUserNew(telefono, code, player_id = '', nombre = '', ciudad = '', sucursal_id = "") {
        var datoaEnviar = {
            "telefono": telefono,
            "codigo": code,
            "player_id": player_id,
            "nombre": nombre,
            "ciudad": ciudad,
            "sucursal_id": sucursal_id
        };
        return this.http.post(this.path + "moto/login-phone-confirm", datoaEnviar, this.httpOptions2)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["tap"])((data) => {
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["of"])(data);
        }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["catchError"])((err) => {
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["throwError"])(err);
        }));
    }
};
ApiService.ctorParameters = () => [
    { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"] }
];
ApiService = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root'
    })
], ApiService);



/***/ }),

/***/ "TyLO":
/*!***********************************************************!*\
  !*** ./src/app/select-sucursal/select-sucursal.module.ts ***!
  \***********************************************************/
/*! exports provided: SelectSucursalPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SelectSucursalPageModule", function() { return SelectSucursalPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "ofXK");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "3Pt+");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "TEn/");
/* harmony import */ var _select_sucursal_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./select-sucursal-routing.module */ "bq9t");
/* harmony import */ var _select_sucursal_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./select-sucursal.page */ "erPI");







let SelectSucursalPageModule = class SelectSucursalPageModule {
};
SelectSucursalPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _select_sucursal_routing_module__WEBPACK_IMPORTED_MODULE_5__["SelectSucursalPageRoutingModule"]
        ],
        declarations: [_select_sucursal_page__WEBPACK_IMPORTED_MODULE_6__["SelectSucursalPage"]]
    })
], SelectSucursalPageModule);



/***/ }),

/***/ "bq9t":
/*!*******************************************************************!*\
  !*** ./src/app/select-sucursal/select-sucursal-routing.module.ts ***!
  \*******************************************************************/
/*! exports provided: SelectSucursalPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SelectSucursalPageRoutingModule", function() { return SelectSucursalPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var _select_sucursal_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./select-sucursal.page */ "erPI");




const routes = [
    {
        path: '',
        component: _select_sucursal_page__WEBPACK_IMPORTED_MODULE_3__["SelectSucursalPage"]
    }
];
let SelectSucursalPageRoutingModule = class SelectSucursalPageRoutingModule {
};
SelectSucursalPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], SelectSucursalPageRoutingModule);



/***/ })

}]);
//# sourceMappingURL=select-sucursal-select-sucursal-module-es2015.js.map