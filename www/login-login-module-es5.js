(function () {
  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

  function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

  function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

  (window["webpackJsonp"] = window["webpackJsonp"] || []).push([["login-login-module"], {
    /***/
    "34Y5": function Y5(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "LoginPage", function () {
        return LoginPage;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "mrSG");
      /* harmony import */


      var _raw_loader_login_page_html__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! raw-loader!./login.page.html */
      "V6Ie");
      /* harmony import */


      var _login_page_scss__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! ./login.page.scss */
      "r67e");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/core */
      "fXoL");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @ionic/angular */
      "TEn/");
      /* harmony import */


      var _ionic_native_onesignal_ngx__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! @ionic-native/onesignal/ngx */
      "wljF");
      /* harmony import */


      var _service_api_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! ../service/api.service */
      "PLN7");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
      /*! @angular/router */
      "tyNb");

      var LoginPage = /*#__PURE__*/function () {
        function LoginPage(toastController, nav, oneSignal, loadingController, service, router) {
          _classCallCheck(this, LoginPage);

          this.toastController = toastController;
          this.nav = nav;
          this.oneSignal = oneSignal;
          this.loadingController = loadingController;
          this.service = service;
          this.router = router;
          this.img = "assets/logo-negativo.png";
        }

        _createClass(LoginPage, [{
          key: "ngOnInit",
          value: function ngOnInit() {
            var _this = this;

            this.oneSignal.startInit('05406fd8-6a53-43ae-ac38-d416ef79aac9', '785164966576');
            this.oneSignal.getIds().then(function (id) {
              _this.player_id = id.userId;
            });
            this.oneSignal.inFocusDisplaying(this.oneSignal.OSInFocusDisplayOption.InAppAlert);
            this.oneSignal.handleNotificationReceived().subscribe(function () {});
            this.oneSignal.handleNotificationOpened().subscribe(function () {});
            this.oneSignal.endInit();
          }
        }, {
          key: "login",
          value: function login() {
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee2() {
              var _this2 = this;

              var loading;
              return regeneratorRuntime.wrap(function _callee2$(_context2) {
                while (1) {
                  switch (_context2.prev = _context2.next) {
                    case 0:
                      this.player_id;
                      _context2.next = 3;
                      return this.loadingController.create({
                        message: 'Cargando....'
                      });

                    case 3:
                      loading = _context2.sent;
                      _context2.next = 6;
                      return loading.present().then(function () {
                        _this2.service.loginUsuario(_this2.email, _this2.password, _this2.player_id).subscribe(function (data) {
                          return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(_this2, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee() {
                            return regeneratorRuntime.wrap(function _callee$(_context) {
                              while (1) {
                                switch (_context.prev = _context.next) {
                                  case 0:
                                    this.success = data.data;
                                    console.log(this.success);
                                    localStorage.setItem('access_token', this.success.access_token);
                                    localStorage.setItem('idUser', this.success.user.id);
                                    localStorage.setItem('idMoto', this.success.user.moto_id);
                                    this.nav.navigateRoot(['/tabs/tab1']); // this.rolIdUsuario = data.type;

                                    _context.next = 8;
                                    return loading.dismiss();

                                  case 8:
                                  case "end":
                                    return _context.stop();
                                }
                              }
                            }, _callee, this);
                          }));
                        }, function (err) {
                          console.log(err);
                          loading.dismiss();

                          _this2.errorRequest(err.error.data.message);
                        });
                      });

                    case 6:
                    case "end":
                      return _context2.stop();
                  }
                }
              }, _callee2, this);
            }));
          }
        }, {
          key: "presentToast",
          value: function presentToast() {
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee3() {
              var toast;
              return regeneratorRuntime.wrap(function _callee3$(_context3) {
                while (1) {
                  switch (_context3.prev = _context3.next) {
                    case 0:
                      _context3.next = 2;
                      return this.toastController.create({
                        message: 'Bienvenido' + ' ' + this.name,
                        duration: 2000
                      });

                    case 2:
                      toast = _context3.sent;
                      this.nav.navigateRoot('/tabs/tab1');
                      toast.present();

                    case 5:
                    case "end":
                      return _context3.stop();
                  }
                }
              }, _callee3, this);
            }));
          }
        }, {
          key: "errorRequest",
          value: function errorRequest(sms) {
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee4() {
              var toast;
              return regeneratorRuntime.wrap(function _callee4$(_context4) {
                while (1) {
                  switch (_context4.prev = _context4.next) {
                    case 0:
                      _context4.next = 2;
                      return this.toastController.create({
                        message: sms,
                        duration: 2000
                      });

                    case 2:
                      toast = _context4.sent;
                      toast.present();

                    case 4:
                    case "end":
                      return _context4.stop();
                  }
                }
              }, _callee4, this);
            }));
          }
        }, {
          key: "register",
          value: function register() {
            this.router.navigate(['/registro']);
          }
        }]);

        return LoginPage;
      }();

      LoginPage.ctorParameters = function () {
        return [{
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["ToastController"]
        }, {
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["NavController"]
        }, {
          type: _ionic_native_onesignal_ngx__WEBPACK_IMPORTED_MODULE_5__["OneSignal"]
        }, {
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["LoadingController"]
        }, {
          type: _service_api_service__WEBPACK_IMPORTED_MODULE_6__["ApiService"]
        }, {
          type: _angular_router__WEBPACK_IMPORTED_MODULE_7__["Router"]
        }];
      };

      LoginPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["Component"])({
        selector: 'app-login',
        template: _raw_loader_login_page_html__WEBPACK_IMPORTED_MODULE_1__["default"],
        styles: [_login_page_scss__WEBPACK_IMPORTED_MODULE_2__["default"]]
      })], LoginPage);
      /***/
    },

    /***/
    "PLN7": function PLN7(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "ApiService", function () {
        return ApiService;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "mrSG");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "fXoL");
      /* harmony import */


      var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/common/http */
      "tk/3");
      /* harmony import */


      var rxjs__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! rxjs */
      "qCKp");
      /* harmony import */


      var rxjs_operators__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! rxjs/operators */
      "kU1M");

      var ApiService = /*#__PURE__*/function () {
        function ApiService(http) {
          _classCallCheck(this, ApiService);

          this.http = http; // path = "https://callcenter.dronebolivia.com/api/";

          this.path = "https://callcenter.rnova-services.com/api/";
          this.items = [];
        }

        _createClass(ApiService, [{
          key: "loginUsuario",
          value: function loginUsuario(email, password, player_id) {
            this.httpOptions = {
              headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]({
                'Content-Type': 'application/json',
                "Accept": "application/json"
              })
            };
            var datoaEnviar = {
              "email": email,
              "password": password,
              "player_id": player_id
            };
            return this.http.post(this.path + "moto/login", datoaEnviar, this.httpOptions).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["tap"])(function (data) {
              return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["of"])(data);
            }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["catchError"])(function (err) {
              return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["throwError"])(err);
            }));
          }
        }, {
          key: "misAsignaciones",
          value: function misAsignaciones() {
            this.accesToken = localStorage.getItem('access_token');
            this.idUsuario = localStorage.getItem('idUser');
            this.httpOptions = {
              headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]({
                'Content-Type': 'application/json',
                "Accept": "application/json",
                'Authorization': 'Bearer ' + this.accesToken
              })
            };
            return this.http.get(this.path + "moto/pedidos", this.httpOptions).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["tap"])(function (data) {
              return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["of"])(data);
            }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["catchError"])(function (err) {
              return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["throwError"])(err);
            }));
          }
        }, {
          key: "sucursalesPanchita",
          value: function sucursalesPanchita() {
            this.accesToken = localStorage.getItem('access_token');
            this.idUsuario = localStorage.getItem('idUser');
            this.httpOptions = {
              headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]({
                'Content-Type': 'application/json',
                "Accept": "application/json",
                'Authorization': 'Bearer ' + this.accesToken
              })
            };
            return this.http.get(this.path + "moto/sucursales", this.httpOptions).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["tap"])(function (data) {
              return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["of"])(data);
            }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["catchError"])(function (err) {
              return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["throwError"])(err);
            }));
          }
        }, {
          key: "actionEntregarPedido",
          value: function actionEntregarPedido(idPedido) {
            this.accesToken = localStorage.getItem('access_token');
            this.userId = localStorage.getItem('idUser');
            this.httpOptions = {
              headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]({
                'Content-Type': 'application/json',
                "Accept": "application/json",
                'Authorization': 'Bearer ' + this.accesToken
              })
            };
            return this.http.post(this.path + "/moto/entregar-pedido?id=" + idPedido, "", this.httpOptions).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["tap"])(function (data) {
              return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["of"])(data);
            }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["catchError"])(function (err) {
              return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["throwError"])(err);
            }));
          }
        }, {
          key: "actionVerPedido",
          value: function actionVerPedido(idPedido) {
            this.accesToken = localStorage.getItem('access_token');
            this.idUsuario = localStorage.getItem('idUser');
            this.httpOptions = {
              headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]({
                'Content-Type': 'application/json',
                "Accept": "application/json",
                'Authorization': 'Bearer ' + this.accesToken
              })
            };
            return this.http.get(this.path + "/moto/ver-pedido?id=" + idPedido, this.httpOptions).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["tap"])(function (data) {
              return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["of"])(data);
            }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["catchError"])(function (err) {
              return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["throwError"])(err);
            }));
          }
        }, {
          key: "perfilMotociclista",
          value: function perfilMotociclista() {
            this.accesToken = localStorage.getItem('access_token');
            this.idUsuario = localStorage.getItem('idUser');
            this.httpOptions = {
              headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]({
                'Content-Type': 'application/json',
                "Accept": "application/json",
                'Authorization': 'Bearer ' + this.accesToken
              })
            };
            return this.http.get(this.path + "moto/perfil", this.httpOptions).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["tap"])(function (data) {
              return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["of"])(data);
            }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["catchError"])(function (err) {
              return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["throwError"])(err);
            }));
          }
        }, {
          key: "cambiarFotografia",
          value: function cambiarFotografia(foto) {
            this.accesToken = localStorage.getItem('access_token');
            this.userId = localStorage.getItem('idUser');
            this.httpOptions = {
              headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]({
                'Content-Type': 'application/json',
                "Accept": "application/json",
                'Authorization': 'Bearer ' + this.accesToken
              })
            };
            var datoaEnviar = {
              "foto": foto
            };
            return this.http.post(this.path + "moto/modificar-foto", datoaEnviar, this.httpOptions).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["tap"])(function (data) {
              return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["of"])(data);
            }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["catchError"])(function (err) {
              return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["throwError"])(err);
            }));
          }
        }, {
          key: "cambiarSucursal",
          value: function cambiarSucursal(sucursalId) {
            this.accesToken = localStorage.getItem('access_token');
            this.userId = localStorage.getItem('idUser');
            this.httpOptions = {
              headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]({
                'Content-Type': 'application/json',
                "Accept": "application/json",
                'Authorization': 'Bearer ' + this.accesToken
              })
            };
            var datoaEnviar = {
              "sucursal_id": sucursalId
            };
            return this.http.post(this.path + "moto/update", datoaEnviar, this.httpOptions).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["tap"])(function (data) {
              return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["of"])(data);
            }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["catchError"])(function (err) {
              return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["throwError"])(err);
            }));
          }
        }, {
          key: "getSms",
          value: function getSms(telefono) {
            var datoaEnviar = {
              "telefono": telefono
            };
            return this.http.post(this.path + "moto/login-phone", datoaEnviar, this.httpOptions2).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["tap"])(function (data) {
              return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["of"])(data);
            }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["catchError"])(function (err) {
              return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["throwError"])(err);
            }));
          }
        }, {
          key: "getRegisterUserNew",
          value: function getRegisterUserNew(telefono, code) {
            var player_id = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : '';
            var nombre = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : '';
            var ciudad = arguments.length > 4 && arguments[4] !== undefined ? arguments[4] : '';
            var sucursal_id = arguments.length > 5 && arguments[5] !== undefined ? arguments[5] : "";
            var datoaEnviar = {
              "telefono": telefono,
              "codigo": code,
              "player_id": player_id,
              "nombre": nombre,
              "ciudad": ciudad,
              "sucursal_id": sucursal_id
            };
            return this.http.post(this.path + "moto/login-phone-confirm", datoaEnviar, this.httpOptions2).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["tap"])(function (data) {
              return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["of"])(data);
            }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["catchError"])(function (err) {
              return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["throwError"])(err);
            }));
          }
        }]);

        return ApiService;
      }();

      ApiService.ctorParameters = function () {
        return [{
          type: _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"]
        }];
      };

      ApiService = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root'
      })], ApiService);
      /***/
    },

    /***/
    "V6Ie": function V6Ie(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "\r\n<ion-content>\r\n<br>\r\n<br>\r\n<div class=\"ion-text-center\">\r\n<img src=\"{{img}}\" alt=\"\">\r\n</div>\r\n\r\n<ion-list>\r\n  <div class=\"ion-text-center\">\r\n    <ion-label   position=\"stacked\" style=\"color: aliceblue; font-size: 1.2em;\">Email</ion-label>\r\n  </div>\r\n  \r\n  <ion-item lines=\"none\">\r\n    <ion-input id=\"input\" class=\"ion-text-center\" placeholder=\"Ingrese su email\" type=\"text\" [(ngModel)]=\"email\"></ion-input>\r\n  </ion-item>\r\n  <br>\r\n  <div class=\"ion-text-center\">\r\n    <ion-label   position=\"stacked\" style=\"color: aliceblue; font-size: 1.2em;\">Contraseña</ion-label>\r\n  </div>\r\n\r\n  <ion-item lines=\"none\">\r\n    \r\n    <ion-input id=\"input\" class=\"ion-text-center\" placeholder=\"Ingrese su contraseña\"  type=\"password\" [(ngModel)]=\"password\" value=\"\"></ion-input>\r\n  </ion-item>\r\n</ion-list>\r\n<div class=\"ion-text-center\">\r\n  <ion-button id=\"button-login\" [disabled]=\"!email || !password\" (click)=\"login()\" shape=\"round\" >\r\n    Ingresar\r\n  </ion-button>\r\n</div>\r\n<p class=\"ion-text-center\">--O--</p>\r\n<br>\r\n<div class=\"ion-text-center\">\r\n  <ion-button id=\"button-login\"  (click)=\"register()\" shape=\"round\" >\r\n    Ingresa con Celular\r\n  </ion-button>\r\n</div>\r\n\r\n<!-- <div class=\"ion-text-center\" id=\"rnova\"> \r\n<p>Development by RNOVA</p>\r\n</div> -->\r\n</ion-content>\r\n";
      /***/
    },

    /***/
    "X3zk": function X3zk(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "LoginPageModule", function () {
        return LoginPageModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "mrSG");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "fXoL");
      /* harmony import */


      var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/common */
      "ofXK");
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/forms */
      "3Pt+");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @ionic/angular */
      "TEn/");
      /* harmony import */


      var _login_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! ./login-routing.module */
      "euwS");
      /* harmony import */


      var _login_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! ./login.page */
      "34Y5");

      var LoginPageModule = function LoginPageModule() {
        _classCallCheck(this, LoginPageModule);
      };

      LoginPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"], _login_routing_module__WEBPACK_IMPORTED_MODULE_5__["LoginPageRoutingModule"]],
        declarations: [_login_page__WEBPACK_IMPORTED_MODULE_6__["LoginPage"]]
      })], LoginPageModule);
      /***/
    },

    /***/
    "euwS": function euwS(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "LoginPageRoutingModule", function () {
        return LoginPageRoutingModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "mrSG");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "fXoL");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/router */
      "tyNb");
      /* harmony import */


      var _login_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! ./login.page */
      "34Y5");

      var routes = [{
        path: '',
        component: _login_page__WEBPACK_IMPORTED_MODULE_3__["LoginPage"]
      }];

      var LoginPageRoutingModule = function LoginPageRoutingModule() {
        _classCallCheck(this, LoginPageRoutingModule);
      };

      LoginPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
      })], LoginPageRoutingModule);
      /***/
    },

    /***/
    "r67e": function r67e(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "img {\n  width: 80%;\n}\n\np {\n  color: white;\n}\n\nion-input {\n  font-size: 1.5em;\n}\n\nion-list {\n  margin: 20px;\n  padding-top: 30px;\n}\n\n#rnova {\n  padding-top: 40%;\n}\n\n#button-login {\n  --background:#df2c41;\n}\n\nion-item {\n  --background: transparent;\n  color: aliceblue;\n}\n\nion-list {\n  background-color: transparent;\n}\n\nion-content {\n  --background:#0d2e3f ;\n}\n\nion-label {\n  font-size: 1.5em;\n}\n\n#input {\n  background: white;\n  color: black;\n  font-size: 0.9em;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uXFwuLlxcLi5cXGxvZ2luLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNJLFVBQUE7QUFDSjs7QUFDQTtFQUNJLFlBQUE7QUFFSjs7QUFBQTtFQUNJLGdCQUFBO0FBR0o7O0FBREE7RUFDSSxZQUFBO0VBQ0EsaUJBQUE7QUFJSjs7QUFGQTtFQUNJLGdCQUFBO0FBS0o7O0FBSEE7RUFDSSxvQkFBQTtBQU1KOztBQUpBO0VBQ0kseUJBQUE7RUFDQSxnQkFBQTtBQU9KOztBQUxBO0VBQ0ksNkJBQUE7QUFRSjs7QUFOQTtFQUNJLHFCQUFBO0FBU0o7O0FBUEE7RUFDSSxnQkFBQTtBQVVKOztBQVJBO0VBQ0ksaUJBQUE7RUFDQSxZQUFBO0VBQ0EsZ0JBQUE7QUFXSiIsImZpbGUiOiJsb2dpbi5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyJpbWd7XHJcbiAgICB3aWR0aDogODAlO1xyXG4gICB9XHJcbnB7XHJcbiAgICBjb2xvcjogd2hpdGU7XHJcbn1cclxuaW9uLWlucHV0e1xyXG4gICAgZm9udC1zaXplOiAxLjVlbTtcclxufVxyXG5pb24tbGlzdHtcclxuICAgIG1hcmdpbjogMjBweDtcclxuICAgIHBhZGRpbmctdG9wOiAzMHB4O1xyXG59XHJcbiNybm92YXtcclxuICAgIHBhZGRpbmctdG9wOiA0MCU7XHJcbn1cclxuI2J1dHRvbi1sb2dpbntcclxuICAgIC0tYmFja2dyb3VuZDojZGYyYzQxO1xyXG59XHJcbmlvbi1pdGVte1xyXG4gICAgLS1iYWNrZ3JvdW5kOiB0cmFuc3BhcmVudDtcclxuICAgIGNvbG9yOiBhbGljZWJsdWU7XHJcbn1cclxuaW9uLWxpc3R7XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOnRyYW5zcGFyZW50O1xyXG59XHJcbmlvbi1jb250ZW50e1xyXG4gICAgLS1iYWNrZ3JvdW5kOiMwZDJlM2YgO1xyXG59XHJcbmlvbi1sYWJlbHtcclxuICAgIGZvbnQtc2l6ZTogMS41ZW07XHJcbn1cclxuI2lucHV0e1xyXG4gICAgYmFja2dyb3VuZDogd2hpdGU7XHJcbiAgICBjb2xvcjogYmxhY2s7XHJcbiAgICBmb250LXNpemU6IDAuOWVtO1xyXG59XHJcblxyXG4iXX0= */";
      /***/
    }
  }]);
})();
//# sourceMappingURL=login-login-module-es5.js.map