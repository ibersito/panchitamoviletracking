import { Component } from '@angular/core';
import { AlertController, ToastController, LoadingController, Platform } from '@ionic/angular';
import { LaunchNavigator, LaunchNavigatorOptions } from '@ionic-native/launch-navigator/ngx';
import { ApiService } from '../service/api.service';
import { Router } from '@angular/router';
import { LocalNotifications } from '@ionic-native/local-notifications/ngx';
import { BackgroundMode } from '@ionic-native/background-mode/ngx';
import { Geolocation } from '@ionic-native/geolocation/ngx';

import firebase from 'firebase';

@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss']
})
export class Tab1Page {


  destination: string;

  dataPaciente: any;
  asignacionesUser: any;
  successPedido: any;
  asignaciones: any[] = [];
  interval: any;
  coords: any[];



  constructor(private alertController: AlertController,
    private toastController: ToastController,
    private launchNavigator: LaunchNavigator,
    public loadingController: LoadingController,
    public service: ApiService,
    public router: Router,
    public backgroundMode: BackgroundMode,
    public platform: Platform,
    public localNotifications: LocalNotifications,
    public geolocation: Geolocation,

    // private backgroundGeolocation: BackgroundGeolocation
  ) {
    platform.ready().then(() => {
      this.backgroundMode.on('activate').subscribe(() => {
        this.backgroundMode.disableWebViewOptimizations();
        // this.backgroundMode.disableBatteryOptimizations();

        console.log("entre aqui hay asignaciones");
        // this.backgroundMode.isScreenOff( () => {
        //   alert('screen off');

        //  });
        clearInterval(this.interval);
        console.log("cerre el intervalo");
        this.interval = setInterval(() => {
          // this.backgroundMode.wakeUp();

          console.log("inicie el intervalo");
          this.trackPosition();
        }, 8000);
        // }

      });
      //   this.backgroundMode.enable();

    })
  }

  trackPosition = () => {
    console.log("entre aqui al intervalo");
    this.showNotification();
  //   this.geolocation
  //     .getCurrentPosition({ maximumAge: 3000, timeout: 5000, enableHighAccuracy: true })
  //     .then(position => {
  //       console.log(position);
  //       this.register(position, localStorage.getItem('idMoto'));

  //     })
  //     .catch(error => {
  //       console.log("error", error);
  //     });
  this.geolocation.watchPosition().subscribe(async(response: any)=>{
    console.log(response);
    
    this.coords = [response.coords.latitude, response.coords.longitude];
    this.register(this.coords[0], this.coords[1], localStorage.getItem('idMoto'));
  })
  };

  ionViewWillEnter() {
    this.loadAsignacionesPedidos();
    this.condition();
  }


  async loadAsignacionesPedidos() {
    const loading = await this.loadingController.create({
      message: 'Cargando....',
      // duration: 3000
    });
    await loading.present().then(() => {
      this.service.misAsignaciones().subscribe(
        async data => {
          this.asignacionesUser = data.data;
          console.log(this.asignacionesUser);

          await loading.dismiss();
        }, err => {
          console.log(err);
          console.log(err);

          loading.dismiss();
        }
      );
    });
  }
  register(lat,lng, userId) {
    console.log(userId);
    console.log(lat,lng);

    // resp.coords.latitude
    // resp.coords.longitude
    firebase.database().ref('locations/' + userId).update({
      lat: lat,
      lng: lng
    });
    console.log('Ubicando');

  }

  async changeEstado(state, item) {
    console.log(state);
    console.log(item.id);
    const alert = await this.alertController.create({
      cssClass: 'my-custom-class',
      header: 'Confirmar!',
      message: 'Realmente desea marcar como entregado el pedido!!',
      buttons: [
        {
          text: 'No',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
            console.log('Confirm Cancel: blah');
            // this.ionViewWillEnter();
          }
        }, {
          text: 'Si',
          handler: () => {
            console.log('Confirm Okay');
            this.service.actionEntregarPedido(item.id).subscribe(
              async data => {
                this.successPedido = data;
                clearInterval(this.interval);
                this.ionViewWillEnter();
                this.presentToast();
                console.log(this.successPedido);
              }, err => {
                console.log(err);

              }
            );
          }
        }
      ]
    });

    await alert.present();
  }


  async presentToast() {
    const toast = await this.toastController.create({
      message: 'Su asignación se cambio a entregado puede verlo en su historial de asignaciones.',
      duration: 2000
    });
    toast.present();
  }

  showLoacte(item) {
    this.destination = item.latitude + ',' + item.longitude;
    // console.log(destination);
    let options: LaunchNavigatorOptions = {
      // start: 'London, ON',
      // app: this.launchNavigator.APP,
      startName: "Hola I am Here"
    }
    this.launchNavigator.navigate(this.destination, options)
      .then(
        success => console.log(success),
        error => console.log('Error launching navigator', error)
      );
  }

  sendDetallePedido(item) {
    console.log(item);
    let idPedido = item.id;
    this.router.navigate(["/detalle-producto", { idPedido: idPedido }]);
  }

  condition() {
    // this.StopBackgroundGeo();
    this.service.misAsignaciones().subscribe(
      data => {
        this.asignaciones = data.data;
        if (this.asignaciones.length) {
          // this.StopBackgroundGeo();
          this.backgroundMode.disable();
          clearInterval(this.interval);
          console.log('entro al stop');
        } else {
          this.backgroundMode.enable();


          console.log('entro al start y activated');
        }
      }
    );
  }

  showNotification() {
    console.log("entre a notificaciones");

    this.localNotifications.schedule({
      text: 'Realizando tracking tienes pedidos asignados cambia su estado para deshabilitar tus notificaciones '
    });

    // this.notificationAlreadyReceived = true;
  }
}


