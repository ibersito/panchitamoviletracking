import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ValidateSmsRegisterPage } from './validate-sms-register.page';

const routes: Routes = [
  {
    path: '',
    component: ValidateSmsRegisterPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ValidateSmsRegisterPageRoutingModule {}
