import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ValidateSmsRegisterPageRoutingModule } from './validate-sms-register-routing.module';

import { ValidateSmsRegisterPage } from './validate-sms-register.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ValidateSmsRegisterPageRoutingModule
  ],
  declarations: [ValidateSmsRegisterPage]
})
export class ValidateSmsRegisterPageModule {}
