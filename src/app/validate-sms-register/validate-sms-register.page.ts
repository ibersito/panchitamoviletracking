import { Component, OnInit,Input } from '@angular/core';
import { ModalController, LoadingController, ToastController, NavController } from '@ionic/angular';
import { ApiService } from 'src/app/service/api.service';
import { OneSignal } from '@ionic-native/onesignal/ngx';

@Component({
  selector: 'app-validate-sms-register',
  templateUrl: './validate-sms-register.page.html',
  styleUrls: ['./validate-sms-register.page.scss'],
})
export class ValidateSmsRegisterPage implements OnInit {
  code:any;
  @Input() telefono = '';
  @Input() message = '';
  @Input() condicion = '';
  nombre: any;
  ciudad: any;
  direccion: any;
  sucursal:any;
    success: any;
    telOficial: any;
  nit: string;
  nitOficial: string;
  sucursales: any;
  player_id: string;
    constructor(public modalCtrl:ModalController,
      private service2:ApiService,
      public loading:LoadingController,
      public toastCtrl:ToastController,
      public nav:NavController,
      public loadingController:LoadingController,
      private oneSignal: OneSignal,
      ) { }

  ngOnInit() {
    this.oneSignal.startInit('05406fd8-6a53-43ae-ac38-d416ef79aac9', '785164966576');
    this.oneSignal.getIds().then((id) => {
      this.player_id =  id.userId;
      console.log(this.player_id);
      
      });
    this.oneSignal.inFocusDisplaying(this.oneSignal.OSInFocusDisplayOption.InAppAlert);
    
    this.oneSignal.handleNotificationReceived().subscribe(() => {
    });
    
    this.oneSignal.handleNotificationOpened().subscribe(() => {
    });
    
    this.oneSignal.endInit();
    console.log(this.telefono,this.message,this.condicion);
  }

  close(){
    this.modalCtrl.dismiss({
      'dismissed': true
    });
  }

  numberOnlyValidation(event: any) {
    const pattern = /[0-9]/;
    let inputChar = String.fromCharCode(event.charCode);
    if (!pattern.test(inputChar)) {
      // invalid character, prevent input
      event.preventDefault();
    }
  }

  async validateCode(){
    this.player_id;
    console.log(this.player_id);
    
    this.telOficial = this.telefono.toString();
    const loading = await this.loading.create({
      message: 'Validando....',
    });
    await loading.present().then(() => {
      this.service2.getRegisterUserNew(this.telOficial,this.code,this.player_id,this.nombre,this.ciudad,"5").subscribe(
        async data => {
          console.log(data);
          this.success = data.data;
          console.log(this.success);
          
          localStorage.setItem('access_token', this.success.access_token);
          localStorage.setItem('idUser', this.success.user.id);
          localStorage.setItem('idMoto', this.success.user.moto_id);

          await loading.dismiss();
          this.modalCtrl.dismiss();
          this.successRequest();
          this.nav.navigateRoot('/tabs/tab1');
        }, err => {
          console.log(err);
          this.errorRequest(err.error.data.message);
          loading.dismiss();
        }
      );
    });
  }

  async validateCode2(){
    this.telOficial = this.telefono.toString();
    const loading = await this.loading.create({
      message: 'Validando....',
    });
    await loading.present().then(() => {
      this.service2.getRegisterUserNew(this.telOficial,this.code,this.player_id,this.nombre,this.ciudad).subscribe(
        async data => {
          console.log(data);
          this.success = data.data;
          localStorage.setItem('access_token', this.success.access_token);
          localStorage.setItem('idUser', this.success.user.id);
          localStorage.setItem('idMoto', this.success.user.moto_id);
          await loading.dismiss();
          this.modalCtrl.dismiss();
          this.successRequest();
          this.nav.navigateRoot('/tabs/tab1');
        }, err => {
          console.log(err);
          this.errorRequest(err.error.data.message);
          loading.dismiss();
        }
      );
    });
  }


  async successRequest() {
    const toast = await this.toastCtrl.create({
      message: 'Bienvenido a Drone Delivery',
      duration: 3000,
      cssClass: 'my-custom-class-success'
    });
    toast.present();
  }

  async errorRequest(sms) {
    const toast = await this.toastCtrl.create({
      message: sms,
      duration: 3000,
      cssClass: 'my-custom-class-alert'
    });
    toast.present();
  }


}
