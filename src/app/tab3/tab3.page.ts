import { Component } from '@angular/core';
import { AlertController, NavController, LoadingController, ModalController } from '@ionic/angular';
import { ApiService } from '../service/api.service';
import { Camera, CameraOptions } from '@ionic-native/camera/ngx';
import { SelectSucursalPage } from '../select-sucursal/select-sucursal.page';

@Component({
  selector: 'app-tab3',
  templateUrl: 'tab3.page.html',
  styleUrls: ['tab3.page.scss']
})
export class Tab3Page {
  img = "assets/icon.png"
  select = "f"
  data ={ ciudad: "",
  email: "",
  estado: "",
  foto: "",
  id: "",
  moto_id: "",
  nombre: "",
  player_id: "",
  rol: "",
  sucursal: {id: "", nombre: "", ciudad: "", detalle: "", estado: ""},
  sucursal_id: "",
  telefono: ""}
  public img2="assets/camara.png";
  imgOficial: string;
  success: any;

  constructor(
  private alertController: AlertController,
  public nav:NavController,
  public loadingController:LoadingController,
  private service:ApiService,
  private camera: Camera,
  public modalController:ModalController
  ) {
    this.select;
  }

  ionViewWillEnter(){
   this.perfil();
  }

  async perfil(){
    const loading = await this.loadingController.create({
      message: 'Cargando....',
      // duration: 3000
    });
    await loading.present().then(() => {
      this.service.perfilMotociclista().subscribe(
        async data => {
          this.data = data.data.user;
          console.log(data);
        
          await loading.dismiss();
        }, err => {
          console.log(err);
          loading.dismiss();
        }
      );
    }); 
  }

  async logout(){
    const alert = await this.alertController.create({
      cssClass: 'my-custom-class',
      header: 'Confirmar!',
      message: 'Realmente desea salir de la App de Delivery!!',
      buttons: [
        {
          text: 'No',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
            console.log('Confirm Cancel: blah');
          }
        }, {
          text: 'Si',
          handler: () => {
            console.log('Confirm Okay');
            this.nav.navigateRoot('/login');
            localStorage.clear();
          }
        }
      ]
    });

    await alert.present();
  }

  fotografia() {
    const options: CameraOptions = {
      quality: 70,
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE,
      correctOrientation: true,
      allowEdit: false,
      targetHeight: 800,
      targetWidth: 1020 
    }
    this.camera.getPicture(options).then((imageData) => {
      this.imgOficial = "data:image/jpeg;base64," + imageData;
      this.editarFotografia();
      }, (err) => {
        console.log(err);
        
      });
  }

  async editarFotografia(){
    const loading = await this.loadingController.create({
      message: 'Cargando....',
      // duration: 3000
    });
    await loading.present().then(() => {
      this.service.cambiarFotografia(this.imgOficial).subscribe(
        async data => {
          console.log(data);
          this.success = data;
          await loading.dismiss();
          this.ionViewWillEnter();
        }, err => {
          console.log(err);
          // this.errorRequest(err.error.message)
          loading.dismiss();
        }
      );
    }); 
  }

  async editar(){
    const modal = await this.modalController.create({
      component: SelectSucursalPage
    });
    modal.onDidDismiss().then(() => {
      this.perfil(); //Update data when closing modal
    });
    return await modal.present();
  }

}
