import { Component, OnInit } from '@angular/core';
import { ToastController, NavController, LoadingController } from '@ionic/angular';
import { OneSignal } from '@ionic-native/onesignal/ngx';
import { ApiService } from '../service/api.service';
import { Router } from '@angular/router';


@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {
  img = "assets/logo-negativo.png"
  name: any;
  password: any;
  player_id: string;
  success: any;
  email: any;
  constructor(private toastController: ToastController,
  public nav:NavController,
  private oneSignal: OneSignal,
  public loadingController:LoadingController,
  public service:ApiService,
  public router:Router
  ) { }

  ngOnInit() {
    this.oneSignal.startInit('05406fd8-6a53-43ae-ac38-d416ef79aac9', '785164966576');
    this.oneSignal.getIds().then((id) => {
      this.player_id =  id.userId;
      });
    this.oneSignal.inFocusDisplaying(this.oneSignal.OSInFocusDisplayOption.InAppAlert);
    
    this.oneSignal.handleNotificationReceived().subscribe(() => {
    });
    
    this.oneSignal.handleNotificationOpened().subscribe(() => {
    });
    
    this.oneSignal.endInit();
  }

  async login(){
    this.player_id;
    const loading = await this.loadingController.create({
      message: 'Cargando....',
      // duration: 3000
    });
    await loading.present().then(() => {
      this.service.loginUsuario(this.email, this.password,this.player_id).subscribe(
        async data => {
          this.success = data.data;
          console.log(this.success);
           localStorage.setItem('access_token', this.success.access_token);
           localStorage.setItem('idUser', this.success.user.id);
           localStorage.setItem('idMoto', this.success.user.moto_id);
          this.nav.navigateRoot(['/tabs/tab1']);
          // this.rolIdUsuario = data.type;
        
          await loading.dismiss();
        }, err => {
          console.log(err);
          loading.dismiss();
          this.errorRequest(err.error.data.message)
        }
      );
    }); 
  }
  async presentToast() {
    const toast = await this.toastController.create({
      message: 'Bienvenido' +' '+ this.name,
      duration: 2000
    });
    this.nav.navigateRoot('/tabs/tab1');
    toast.present();
  }

  async errorRequest(sms) {
    const toast = await this.toastController.create({
      message: sms,
      duration: 2000
    });
    toast.present();
  }

  register(){
    this.router.navigate(['/registro'])
  }

}
