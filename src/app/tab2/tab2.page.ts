import { Component } from '@angular/core';
import { AlertController, NavController, LoadingController } from '@ionic/angular';
import { ApiService } from '../service/api.service';
import { Router } from '@angular/router';
import { Geolocation } from '@ionic-native/geolocation/ngx';

@Component({
  selector: 'app-tab2',
  templateUrl: 'tab2.page.html',
  styleUrls: ['tab2.page.scss']
})
export class Tab2Page {
  historial = [
    {cliente:"Cliente: Dr- Pablo Aranda",direccion:"Avenida Petrolera km 3 1/2",pedido:" 1 cuartos Pollos ",fecha:"07/6/2020 16:42"},
    {cliente:"Cliente: Dr- Gonzalo Pereda",direccion:"Avenida Suecia km 3 1/2",pedido:" 4 cuartos Pollos ",fecha:"07/6/2020 15:42"},
    {cliente:"Cliente: Ing- Jorge Veizaga",direccion:"Avenida Libertador km 3 1/2",pedido:" 4 cuartos Pollos ",fecha:"07/6/2020 14:42"},
   
  ]
  data = { ciudad: "",
  email: "",
  estado: "",
  foto: "",
  id: "",
  moto_id: "",
  nombre: "",
  player_id: "",
  rol: "",
  sucursal: {id: "", nombre: "", ciudad: "", detalle: "", estado: ""},
  sucursal_id: "",
  telefono: ""}
  asignacionesUser: any;
  interval:any;
  constructor(
    private alertController: AlertController,
    public nav:NavController,
    public loadingController:LoadingController,
    private service:ApiService,
    public router:Router,
    public geolocation: Geolocation,


    ) {
     
    }

  ionViewWillEnter(){
    this.perfil();
   }
 
   async perfil(){
     const loading = await this.loadingController.create({
       message: 'Cargando....',
       // duration: 3000
     });
     await loading.present().then(() => {
       this.service.perfilMotociclista().subscribe(
         async data => {
           this.asignacionesUser = data.data.pedidos;
           console.log(this.asignacionesUser);
         
           await loading.dismiss();
         }, err => {
           console.log(err);
           loading.dismiss();
         }
       );
     }); 
   }

   sendDetallePedido(item){
    console.log(item);
    let idPedido = item.id;
    this.router.navigate(["/detalle-producto",{idPedido:idPedido}]);
  }
}
