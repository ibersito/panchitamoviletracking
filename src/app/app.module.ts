import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LaunchNavigator, LaunchNavigatorOptions } from '@ionic-native/launch-navigator/ngx';
import { OneSignal } from '@ionic-native/onesignal/ngx';
import { HttpClientModule } from "@angular/common/http";
import { Camera, CameraOptions } from '@ionic-native/camera/ngx';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';
import { CallNumber } from '@ionic-native/call-number/ngx';
import { AndroidPermissions } from '@ionic-native/android-permissions/ngx';

import { BackgroundMode } from '@ionic-native/background-mode/ngx';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import { LocalNotifications } from '@ionic-native/local-notifications/ngx';
// import { LocationAccuracy } from '@ionic-native/location-accuracy/ngx';

import firebase from 'firebase';
const FIREBASE = {
  apiKey: 'AIzaSyARM5jGbwVoyAXR04l3DTxvxtOKsZ3s1wQ',
  authDomain: 'trackingdronedelivery.firebaseapp.com',
  databaseURL: 'https://trackingdronedelivery.firebaseio.com',
  projectId: 'trackingdronedelivery',
  storageBucket: 'trackingdronedelivery.appspot.com',
  messagingSenderId: '98277039324',
  appId: '1:98277039324:web:41808b32c4f3c0fd436819',
  measurementId: 'G-DMJ9J47B50'
};
firebase.initializeApp(FIREBASE);

@NgModule({
  declarations: [AppComponent],
  entryComponents: [],
  imports: [HttpClientModule,BrowserModule, IonicModule.forRoot(), AppRoutingModule],
  providers: [
    BackgroundMode,
    Geolocation,
    LocalNotifications,
    AndroidPermissions,
    CallNumber,
    InAppBrowser,
    Camera,
    OneSignal,
    LaunchNavigator,
    StatusBar,
    SplashScreen,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
