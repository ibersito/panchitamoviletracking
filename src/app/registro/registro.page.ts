import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { LoadingController, ModalController, NavController, ToastController } from '@ionic/angular';
import { ApiService } from '../service/api.service';
import { ValidateSmsRegisterPage } from '../validate-sms-register/validate-sms-register.page';

@Component({
  selector: 'app-registro',
  templateUrl: './registro.page.html',
  styleUrls: ['./registro.page.scss'],
})
export class RegistroPage implements OnInit {
  form: FormGroup;
  valid = false;
  success: any;
  cont: number;
  ci: any;
  phone: any;
  text: any;
  string: number;
  messageExiste: string;
  formRegistroShow: boolean;

  constructor(public nav: NavController, public formBuilder: FormBuilder,
              public toastCtrl: ToastController, private service: ApiService,
              public loading: LoadingController, public modalController:ModalController
  ) {
    this.cont = 0;
  }

  ngOnInit() {
    this.form = this.formBuilder.group({
      phone: ['', Validators.compose([Validators.required, Validators.max(79999999), Validators.min(59999999), Validators.pattern('[0-9]*')])],
    });
  }

  numberOnlyValidation(event: any) {
    const pattern = /[0-9]/;
    let inputChar = String.fromCharCode(event.charCode);
    if (!pattern.test(inputChar)) {
      // invalid character, prevent input
      event.preventDefault();
    }
  }

  /*
  Funcion send register system client and send page locate
  */
  async registerClient() {
    this.busqueda();
    if (this.cont == 1) {
      this.errorRequest('Falta ' + (8 - this.text ) + ' dígito(s)');
    } else if (this.cont == 2) {
      this.errorRequest('El número ingresado no es válido');
    } else if (this.cont == 3) {
      this.errorRequest('Ese número no existe en el país case');
    } else {
      const loading = await this.loading.create({
        message: 'Verificando....',
      });
      await loading.present().then(() => {
        this.ci = this.form.value.ci;
        this.phone = this.form.value.phone;
        this.service.getSms(this.phone.toString()).subscribe(
            async data => {
              this.success = data.data;
              console.log(this.success);
                this.messageExiste = "El número del celular ya esta registrado";
                const modal = await this.modalController.create({
                  component: ValidateSmsRegisterPage,
                  componentProps: {
                    telefono: this.success.telefono,
                    message: this.messageExiste,
                    condicion:this.success.existe
                  },
                });
                await loading.dismiss();
                return await modal.present();
              // this.successRequest();
              // this.nav.navigateRoot('/tabs/tab1');
            }, err => {
              this.errorRequest(err.error.data.message);
              loading.dismiss();
            }
          );
      });
    }
  }

  async successRequest() {
    const toast = await this.toastCtrl.create({
      message: 'Bienvenido a Doña Panchita',
      duration: 3000,
      cssClass: 'my-custom-class-success'
    });
    toast.present();
  }

  public checkError = (controlName: string, errorName: string) => {
    return this.form.controls[controlName].hasError(errorName);
  }

  async errorRequest(sms) {
    const toast = await this.toastCtrl.create({
      message: sms,
      duration: 3000,
      cssClass: 'my-custom-class-alert'
    });
    toast.present();
  }

  /*
  Funcion send login page
  */
  async sendlogin() {
    this.nav.navigateRoot('login');
  }

  busqueda() {
    this.text = 0;
    this.cont = 0;
    var palabra = (this.form.value.phone).toString();
    var arreglo = palabra.split("");
    console.log(arreglo);
    if (arreglo.length === 8) {
      this.secuencial(".", arreglo)
    } else if(arreglo.length > 8) {
      this.cont = 3;
    } else if (arreglo.length < 8) {
      this.cont = 1;
      this.text = arreglo.length;
    }
  }

  secuencial(value, list) {
    this.cont = 0;
    let found = false;
    let position = -1;
    let index = 0;
 
    while(!found && index < list.length) {
        if(list[index] == value) {
            found = true;
            position = index;
            this.cont = 2;
        } else {
            index += 1;
        }
    }
    return position;
  }

  onChangeTime(e) {
    this.string = (8 - e.detail.value.length);
  }
}
