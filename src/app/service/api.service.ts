import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Observable, throwError, of } from 'rxjs';
import { map, tap, catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ApiService {
  // path = "https://callcenter.dronebolivia.com/api/";
    path = "https://callcenter.rnova-services.com/api/";

  
  idUsuario: string;
  items = [];

  accesToken: string;
  httpOptions: { headers: HttpHeaders; };
  httpOptions2: { headers: HttpHeaders; };
  userId: string;

  constructor(private http: HttpClient) {} 

  loginUsuario(email, password, player_id):Observable<any>{
    this.httpOptions = {
      headers: new HttpHeaders({ 
        'Content-Type': 'application/json', 
        "Accept": "application/json",
      }),
    };
    var datoaEnviar = {
      "email":email,
      "password":password,
      "player_id":player_id
    }
    return this.http.post(this.path +"moto/login", datoaEnviar, this.httpOptions)
      .pipe(
        tap( (data:any)=>{
          return of(data);
        } ),
        catchError( (err)=>{
          return throwError(err);
        } )
      );
  }

  misAsignaciones():Observable<any>{
    this.accesToken = localStorage.getItem('access_token');
    this.idUsuario = localStorage.getItem('idUser');
  
    this.httpOptions = {
      headers: new HttpHeaders({ 
        'Content-Type': 'application/json', 
        "Accept": "application/json",
        'Authorization':'Bearer '+ this.accesToken
      }),
    };
   
    return this.http.get(this.path +"moto/pedidos" ,  this.httpOptions)
      .pipe(
        tap( (data:any)=>{
          return of(data);
        } ),
        catchError( (err)=>{
          return throwError(err);
        } )
      );
  }

  sucursalesPanchita():Observable<any>{
    this.accesToken = localStorage.getItem('access_token');
    this.idUsuario = localStorage.getItem('idUser');
  
    this.httpOptions = {
      headers: new HttpHeaders({ 
        'Content-Type': 'application/json', 
        "Accept": "application/json",
        'Authorization':'Bearer '+ this.accesToken
      }),
    };
   
    return this.http.get(this.path +"moto/sucursales" ,  this.httpOptions)
      .pipe(
        tap( (data:any)=>{
          return of(data);
        } ),
        catchError( (err)=>{
          return throwError(err);
        } )
      );
  }

  actionEntregarPedido(idPedido){
    this.accesToken = localStorage.getItem('access_token');  
    this.userId = localStorage.getItem('idUser');  
    this.httpOptions = {
      headers: new HttpHeaders({ 
        'Content-Type': 'application/json', 
        "Accept": "application/json",
        'Authorization':'Bearer '+ this.accesToken
      }),
    };
    
    return this.http.post(this.path +"/moto/entregar-pedido?id=" + idPedido, "", this.httpOptions)
      .pipe(
        tap( (data:any)=>{
          return of(data);
        } ),
        catchError( (err)=>{
          return throwError(err);
        } )
      ); 
  }

  actionVerPedido(idPedido){
    this.accesToken = localStorage.getItem('access_token');
    this.idUsuario = localStorage.getItem('idUser');
  
    this.httpOptions = {
      headers: new HttpHeaders({ 
        'Content-Type': 'application/json', 
        "Accept": "application/json",
        'Authorization':'Bearer '+ this.accesToken
      }),
    };
   
    return this.http.get(this.path +"/moto/ver-pedido?id=" + idPedido,  this.httpOptions)
      .pipe(
        tap( (data:any)=>{
          return of(data);
        } ),
        catchError( (err)=>{
          return throwError(err);
        } )
      );
  }

  perfilMotociclista(){
    this.accesToken = localStorage.getItem('access_token');
    this.idUsuario = localStorage.getItem('idUser');
  
    this.httpOptions = {
      headers: new HttpHeaders({ 
        'Content-Type': 'application/json', 
        "Accept": "application/json",
        'Authorization':'Bearer '+ this.accesToken
      }),
    };
   
    return this.http.get(this.path +"moto/perfil",  this.httpOptions)
      .pipe(
        tap( (data:any)=>{
          return of(data);
        } ),
        catchError( (err)=>{
          return throwError(err);
        } )
      );
  }

  cambiarFotografia(foto){
    this.accesToken = localStorage.getItem('access_token');  
    this.userId = localStorage.getItem('idUser');  
    this.httpOptions = {
      headers: new HttpHeaders({ 
        'Content-Type': 'application/json', 
        "Accept": "application/json",
        'Authorization':'Bearer '+ this.accesToken
      }),
    };
    var datoaEnviar = {
      "foto":foto,
    }
    return this.http.post(this.path +"moto/modificar-foto", datoaEnviar, this.httpOptions)
      .pipe(
        tap( (data:any)=>{
          return of(data);
        } ),
        catchError( (err)=>{
          return throwError(err);
        } )
      ); 
  }

  cambiarSucursal(sucursalId){
    this.accesToken = localStorage.getItem('access_token');  
    this.userId = localStorage.getItem('idUser');  
    this.httpOptions = {
      headers: new HttpHeaders({ 
        'Content-Type': 'application/json', 
        "Accept": "application/json",
        'Authorization':'Bearer '+ this.accesToken
      }),
    };
    var datoaEnviar = {
      "sucursal_id":sucursalId,
    }
    return this.http.post(this.path +"moto/update", datoaEnviar, this.httpOptions)
      .pipe(
        tap( (data:any)=>{
          return of(data);
        } ),
        catchError( (err)=>{
          return throwError(err);
        } )
      ); 
  }

  getSms(telefono):Observable<any>{
    var datoaEnviar = {
      "telefono": telefono
    }
    return this.http.post(this.path +"moto/login-phone", datoaEnviar, this.httpOptions2)
      .pipe(
        tap( (data:any)=>{
          return of(data);
        } ),
        catchError( (err)=>{
          return throwError(err);
        } )
      );
  }

  getRegisterUserNew(telefono,code,player_id='',nombre='',ciudad='',sucursal_id=""):Observable<any>{
    var datoaEnviar = {
      "telefono": telefono,
      "codigo": code,
      "player_id":player_id,
      "nombre": nombre, // si el cliente es nuevo
      "ciudad": ciudad, // si el cliente es nuevo
      "sucursal_id":sucursal_id
    }
    return this.http.post(this.path +"moto/login-phone-confirm", datoaEnviar, this.httpOptions2)
      .pipe(
        tap( (data:any)=>{
          return of(data);
        } ),
        catchError( (err)=>{
          return throwError(err);
        } )
      );
  }
}
