import { Component } from '@angular/core';

import { Platform, NavController } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { AndroidPermissions } from '@ionic-native/android-permissions/ngx';
import { BackgroundMode } from '@ionic-native/background-mode/ngx';
// import { LocationAccuracy } from '@ionic-native/location-accuracy/ngx';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})
export class AppComponent {
  player_id: string;
  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private navCtrl: NavController,
    private androidPermissions: AndroidPermissions,
    public backgroundMode: BackgroundMode,
    // private locationAccuracy: LocationAccuracy

  ) {
    this.initializeApp();
  }

  initializeApp() {
    this.platform.ready().then(() => {
      // this.backgroundMode.on('activate').subscribe(() => {
      //   console.log('activated');
      // });
      // this.backgroundMode.setDefaults({ title: "Ionic team", text: "Please fix that" });
      this.backgroundMode.setDefaults({ silent: true });
      // this.backgroundMode.enable();
      const data = this.androidPermissions.requestPermissions([
        "android.permission.ACCESS_BACKGROUND_LOCATION",
        "android.permission.ACCESS_COARSE_LOCATION",
        this.androidPermissions.PERMISSION.ACCESS_FINE_LOCATION,
      ]);
      if(localStorage.getItem('access_token')) { 
        this.navCtrl.navigateRoot('/tabs/tab1');
      } else { 
       this.navCtrl.navigateRoot('/login');
      }
      this.statusBar.backgroundColorByHexString('#15242a');
      this.splashScreen.hide();
    });
  }
}
