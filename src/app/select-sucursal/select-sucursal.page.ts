import { Component, OnInit } from '@angular/core';
import { ModalController, LoadingController, AlertController } from '@ionic/angular';
import { ApiService } from '../service/api.service';
import { alertController } from '@ionic/core';
import { Alert } from 'selenium-webdriver';

@Component({
  selector: 'app-select-sucursal',
  templateUrl: './select-sucursal.page.html',
  styleUrls: ['./select-sucursal.page.scss'],
})
export class SelectSucursalPage implements OnInit {
  data: any;
  img = "assets/logooficial.jpg"
  successChangeMoto: any;

  constructor(public modal:ModalController,
  public loadingController:LoadingController,
  private service:ApiService,
  public alertController:AlertController) { }

  async ngOnInit() {
    const loading = await this.loadingController.create({
      message: 'Cargando....',
      // duration: 3000
    });
    await loading.present().then(() => {
      this.service.sucursalesPanchita().subscribe(
        async data => {
          this.data = data.data;
          console.log(data);
        
          await loading.dismiss();
        }, err => {
          console.log(err);
          loading.dismiss();
        }
      );
    }); 
  }

  closeModalEditProfile() {
    this.modal.dismiss({
      'dismissed': true
    });
  }

  async seleccionarSucursal(item){
    console.log(item);
    const alert = await this.alertController.create({
      header: 'Confirmar!',
      message: 'Desea cambiar de sucursal?',
      buttons: [
        {
          text: 'No',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
            console.log('Confirm Cancel: blah');
          }
        }, {
          text: 'si',
          handler: () => {
            this.service.cambiarSucursal(item.id).subscribe(
              async data => {
                this.successChangeMoto = data;
                console.log(this.successChangeMoto);
                this.modal.dismiss({
                  'dismissed': true
                });
              }, err => {
                console.log(err);
              }
            );  
          }
        }
      ]
    });

    await alert.present();
  }

}
