import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { SelectSucursalPage } from './select-sucursal.page';

describe('SelectSucursalPage', () => {
  let component: SelectSucursalPage;
  let fixture: ComponentFixture<SelectSucursalPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SelectSucursalPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(SelectSucursalPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
