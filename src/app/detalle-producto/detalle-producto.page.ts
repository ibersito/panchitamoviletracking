import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { LoadingController } from '@ionic/angular';
import { ApiService } from '../service/api.service';
import { InAppBrowser, InAppBrowserOptions} from '@ionic-native/in-app-browser/ngx';
import { CallNumber } from '@ionic-native/call-number/ngx';

@Component({
  selector: 'app-detalle-producto',
  templateUrl: './detalle-producto.page.html',
  styleUrls: ['./detalle-producto.page.scss'],
})
export class DetalleProductoPage implements OnInit {
  idPedido: any;
  data = {
    id:"" ,
    fecha_entrega: "",
    hora_entrega: "",
    fecha_despacho: "",
    hora_despacho: "",
    fecha_entregado:"",
    hora_entregado:"",
    cliente_id: "",
    cliente: {
        id: "",
        nit: "",
        telefono: "",
        zona: "",
        direccion:"",
        razon_social: "",
        zoom: "",
        foto: null,
        latitude: "",
        longitude: ""
    },
    telefono: "",
    zona: "",
    direccion: "",
    latitude: "",
    longitude: "",
    zoom: "",
    instrucciones: "",
    precio_delivery_id: "",
    precio_delivery: {
        id: "",
        nombre: "",
        descripcion: "",
        precio: "",
        estado: ""
    },
    sucursal_delivery_id: "",
    sucursal_delivery: {
        id: "",
        nombre: "",
        ciudad: "",
        detalle: "",
        estado: ""
    },
    tipo_pedido_id: "",
    tipo_pedido: {
        id: "",
        nombre: "",
        descripcion: "",
        estado: ""
    },
    estado: "",
    strEstado: "",
    url_mapa: "",
    detalle: [
        {
            id: "",
            producto_id: "",
            producto: {
                id: "",
                producto: "",
                foto: "",
                detalle: "",
                costo: "",
                estado: "",
                categoria_producto_id: "",
                categoria_producto: {
                    id: "",
                    nombre: "",
                    producto: "",
                    estado: ""
                }
            },
            subtotal: "",
            cantidad: "",
            observacion: ""
        }
    ],
    total:""
  };
  options : InAppBrowserOptions = {
  location : 'yes',//Or 'no' 
  hidden : 'no', //Or  'yes'
  clearcache : 'yes',
  clearsessioncache : 'yes',
  zoom : 'yes',//Android only ,shows browser zoom controls 
  hardwareback : 'yes',
  mediaPlaybackRequiresUserAction : 'no',
  shouldPauseOnSuspend : 'no', //Android only 
  closebuttoncaption : 'Close', //iOS only
  disallowoverscroll : 'no', //iOS only 
  toolbar : 'yes', //iOS only 
  enableViewportScale : 'no', //iOS only 
  allowInlineMediaPlayback : 'no',//iOS only 
  presentationstyle : 'pagesheet',//iOS only 
  fullscreen : 'yes',//Windows only    
  };
  constructor(
  public route:ActivatedRoute,
  public loadingController:LoadingController,
  private service:ApiService,
  private iab: InAppBrowser,
  private callNumber: CallNumber
  ) {
    this.idPedido = this.route.snapshot.paramMap.get('idPedido');
   }

  async ngOnInit() {
    const loading = await this.loadingController.create({
      message: 'Cargando....',
      // duration: 3000
    });
    await loading.present().then(() => {
      this.service.actionVerPedido(this.idPedido).subscribe(
        async data => {
          this.data = data.data;
          console.log(this.data);
        
          await loading.dismiss();
        }, err => {
          console.log(err);
          loading.dismiss();
        }
      );
    }); 
  }

  watshapp(){
    this.iab.create('https://api.whatsapp.com/send?phone=591'+ this.data.telefono+'&text=Hola tu pedido ya esta aqui', `_system`);
  }

  callnumber(){
    this.callNumber.callNumber(this.data.telefono, true)
    .then(res => console.log('Launched dialer!', res))
    .catch(err => console.log('Error launching dialer', err));
  }
}
